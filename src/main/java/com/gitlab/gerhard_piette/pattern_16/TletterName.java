package com.gitlab.gerhard_piette.pattern_16;

import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;

public class TletterName {

	public static Oletter transport(String name, Data dt, int offset) throws DefectLetter, DefectOffset, DefectPattern, DefectLimit {
		var ret = new Oletter();
		var off = offset;
		var oName = Tname.transport(name, dt, off);
		if (!oName.match) {
			ret.match = false;
			return ret;
		}
		dt.addUsedPattern(name, oName.name);
		ret.name = oName.name;
		ret.end = oName.end;
		ret.match = true;
		return ret;
	}

}
