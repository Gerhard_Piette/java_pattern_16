package com.gitlab.gerhard_piette.pattern_16;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OmaxStatement {

	public List<Ostatement> statementList = new ArrayList<>();

	public HashMap<String, Ostatement> statementMap = new HashMap<>();

	public String toString() {
		var ret = "";
		for (var stat : statementList) {
			ret += stat + "\n";
		}
		return ret;
	}
}
