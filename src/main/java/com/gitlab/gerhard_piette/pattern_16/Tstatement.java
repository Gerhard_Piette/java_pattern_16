package com.gitlab.gerhard_piette.pattern_16;

import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;

public class Tstatement {

	public static Ostatement transport(Data dt, int offset) throws DefectLetter, DefectOffset, DefectPattern, DefectLimit {
		var ret = new Ostatement();
		ret.begin = offset;
		var off = offset;
		if (off == dt.text.getLength()) {
			ret.match = false;
			return ret;
		}
		var oDef = Tcomment.transport(dt, off);
		if (oDef.match) {
			ret.oDef = oDef;
			ret.match = true;
			ret.end = oDef.end;
			return ret;
		}
		Oname oName = Tname.transport("", dt, off);
		if (!oName.match) {
			var ms = "Defect pattern: Invalid pattern name after offset: " + off + ".";
			throw new DefectPattern(ms);
		}
		off = oName.end;
		off = dt.afterSpace(off);
		oDef = TdefGlobal.transport(oName.name, dt, off);
		if (oDef.match) {
			ret.name = oName.name;
			ret.oDef = oDef;
			ret.match = true;
			ret.end = oDef.end;
			return ret;
		}
		var ms = "Defect pattern: " + oName.name + ". Invalid definition.";
		throw new DefectPattern(ms);
	}

}