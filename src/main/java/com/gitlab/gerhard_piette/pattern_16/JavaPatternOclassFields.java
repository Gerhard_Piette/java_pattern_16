package com.gitlab.gerhard_piette.pattern_16;

import com.gitlab.gerhard_piette.java_string_1.JavaString;
import com.gitlab.gerhard_piette.text_6.Text;

public class JavaPatternOclassFields {

	public JavaPattern jp;

	public JavaPatternOclassFields(JavaPattern jp) {
		this.jp = jp;
	}

	public String makeLine(int indentLevel, String line) {
		return jp.makeLine(indentLevel, line);
	}

	public String makeOclassName(String name) {
		return jp.makeOclassName(name);
	}

	public String makeOvalueName(String name) {
		return jp.makeOvalueName(name);
	}

	public String makeFields(Ostatement stat, OmaxStatement omaxStatement, Data data) throws DefectGenerator {
		Text keyword = stat.oDef.keyword;
		if (keyword == data.keywordAll) {
			return makeAll(stat, omaxStatement, data);
		}
		if (keyword == data.keywordCheck) {
			return makeCheck(stat, omaxStatement, data);
		}
		if (keyword == data.keywordCopy) {
			return makeCopy(stat, omaxStatement, data);
		}
		if (keyword == data.keywordEnd) {
			return makeEnd(stat, omaxStatement, data);
		}
		if (keyword == data.keywordMax) {
			return makeMax(stat, omaxStatement, data);
		}
		if (keyword == data.keywordMaxto) {
			return makeMaxTo(stat, omaxStatement, data);
		}
		if (keyword == data.keywordMaybe) {
			return makeMaybe(stat, omaxStatement, data);
		}
		if (keyword == data.keywordMin) {
			return makeMin(stat, omaxStatement, data);
		}
		if (keyword == data.keywordMinto) {
			return makeMinTo(stat, omaxStatement, data);
		}
		if (keyword == data.keywordNot) {
			return makeNot(stat, omaxStatement, data);
		}
		if (keyword == data.keywordOne) {
			return makeOne(stat, omaxStatement, data);
		}
		if (keyword == data.keywordText) {
			return makeText(stat, omaxStatement, data);
		}
		if (keyword == data.keywordEqual) {
			return makeLetter(stat, omaxStatement, data);
		}
		if (keyword == data.keywordUnequal) {
			return makeLetter(stat, omaxStatement, data);
		}
		if (keyword == data.keywordPointyLeft) {
			return makeLetter(stat, omaxStatement, data);
		}
		if (keyword == data.keywordPointyRight) {
			return makeLetter(stat, omaxStatement, data);
		}
		if (keyword == data.keywordEqualPointyLeft) {
			return makeLetter(stat, omaxStatement, data);
		}
		if (keyword == data.keywordPointyRightEqual) {
			return makeLetter(stat, omaxStatement, data);
		}
		if (keyword == data.keywordPointy1) {
			return makeLetter(stat, omaxStatement, data);
		}
		if (keyword == data.keywordPointy2) {
			return makeLetter(stat, omaxStatement, data);
		}
		if (keyword == data.keywordPointyEqual1) {
			return makeLetter(stat, omaxStatement, data);
		}
		if (keyword == data.keywordPointyEqual2) {
			return makeLetter(stat, omaxStatement, data);
		}
		var ms = "Keyword not supported: " + keyword + ".";
		throw new DefectGenerator(ms);
	}

	public String makeAll(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		jp.setLocalName(stat.oDef.defList);
		for (var odef : stat.oDef.defList) {
			ret += makeLine(0, "");
			ret += makeLine(1, "public " + makeOclassName(odef.name) + " " + makeOvalueName(odef.localName) + " = null;");
		}
		return ret;
	}

	public String makeCheck(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		for (var odef : stat.oDef.defList) {
			var name = odef.name;
			ret += makeLine(1, "public " + makeOclassName(name) + " " + makeOvalueName(name) + " = null;");
			ret += makeLine(0, "");
		}
		return ret;
	}

	public String makeCopy(Ostatement stat, OmaxStatement omaxStatement, Data data) throws DefectGenerator {
//		var ret = "";
//		for (var odef : stat.oDef.defList) {
//			var name = odef.name;
//			var stat2 = omaxStatement.statementMap.get(name);
//			ret += makeOclassBasicFields(stat2, omaxStatement, data);
//		}
//		return ret;
		var ret = "";
		for (var odef : stat.oDef.defList) {
			var name = odef.name;
			ret += makeLine(1, "public " + makeOclassName(name) + " " + makeOvalueName(name) + " = null;");
			ret += makeLine(0, "");
		}
		return ret;
	}

	public String makeEnd(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		ret += makeLine(1, "public int offset = -1;");
		return ret;
	}


	public String makeMax(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		var name = stat.oDef.defList.get(0).name;
		ret += makeLine(1, "public List<" + makeOclassName(name) + "> " + makeOvalueName(name) + "List = new ArrayList<>();");
		return ret;
	}

	public String makeMaxTo(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		var name = stat.oDef.defList.get(0).name;
		ret += makeLine(1, "public List<" + makeOclassName(name) + "> " + makeOvalueName(name) + "List = new ArrayList<>();");
		ret += makeLine(0, "");
		name = stat.oDef.defList.get(1).name;
		ret += makeLine(1, "public " + makeOclassName(name) + " " + makeOvalueName(name) + " = null;");
		return ret;
	}

	public String makeMaybe(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		var name = stat.oDef.defList.get(0).name;
		ret += makeLine(1, "public " + makeOclassName(name) + " " + makeOvalueName(name) + " = null;");
		return ret;
	}

	public String makeMin(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		var name = stat.oDef.defList.get(0).name;
		ret += makeLine(1, "public List<" + makeOclassName(name) + "> " + makeOvalueName(name) + "List = new ArrayList<>();");
		return ret;
	}

	public String makeMinTo(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		var name = stat.oDef.defList.get(0).name;
		ret += makeLine(1, "public List<" + makeOclassName(name) + "> " + makeOvalueName(name) + "List = new ArrayList<>();");
		ret += makeLine(0, "");
		name = stat.oDef.defList.get(1).name;
		ret += makeLine(1, "public " + makeOclassName(name) + " " + makeOvalueName(name) + " = null;");
		return ret;
	}

	public String makeNot(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		var name = stat.oDef.defList.get(0).name;
		ret += makeLine(1, "public " + makeOclassName(name) + " " + makeOvalueName(name) + " = null;");
		return ret;
	}

	public String makeOne(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		//Duplicates should be indicated as errors in the generated code because they are useless for the "one" pattern.
		for (var odef : stat.oDef.defList) {
			ret += makeLine(0, "");
			var name = odef.name;
			ret += makeLine(1, "public " + makeOclassName(name) + " " + makeOvalueName(name) + " = null;");
		}
		return ret;
	}

	public String makeText(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		var str = JavaString.addEscapeSequences(stat.oDef.str);
		ret += makeLine(1, "public static Text text = new StringText(\"" + str + "\");");
		return ret;
	}

	public String makeLetter(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		ret += makeLine(1, "public int letter = -1;");
		return ret;
	}
}
