package com.gitlab.gerhard_piette.pattern_16;

import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;

public class TdefUnequal {

	public static Odef transport(String name, Data dt, int offset) throws DefectLetter, DefectOffset, DefectPattern, DefectLimit {
		var ret = new Odef();
		ret.keyword = dt.keywordUnequal;
		ret.begin = offset;
		var off = offset;
		//letter 1off = dt.afterSpace(off);
		var afterKey = dt.afterMatch(off, dt.keywordDivEqual);
		if (afterKey < 0) {
			afterKey = dt.afterMatch(off, dt.keywordExclamationEqual);
			if (afterKey < 0) {
				ret.match = false;
				return ret;
			}
		}
		off = afterKey;
		off = dt.afterSpace(off);
		var odefLetter = Tletter.transport(name, dt, off);
		if (odefLetter.match) {
			ret.letter = odefLetter;
		} else {
			var ms = "Defect pattern: " + name + ". Lack of letter declaration after offset " + off + ".";
			throw new DefectPattern(ms);
		}
		off = odefLetter.end;
		ret.end = off;
		ret.match = true;
		return ret;
	}

}