package com.gitlab.gerhard_piette.pattern_16;

import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.java_string_1.JavaString;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.text_6.StringText;
import com.gitlab.gerhard_piette.text_6.Text;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Closed in the sense that the generated Java code is complete.
 * This is useful if the chosen implementation is wanted.
 * <p>
 * Copied from /code/kotlin/pattern_0_4.
 * Transported to Java.
 * Completed.
 */
public class JavaPattern {

	public String iClassFolderName = "iclass";

	public boolean overrideOclassFile = false;

	public JavaPatternOclassFields jpFields;

	public JavaPatternOclassToStringMethod jpToString;

	public JavaPatternIclassReadMethod jpRead;

	public JavaPatternOclassWriteMethod jpWrite;

	public JavaPattern() {
		jpFields = new JavaPatternOclassFields(this);
		jpToString = new JavaPatternOclassToStringMethod(this);
		jpRead = new JavaPatternIclassReadMethod(this);
		jpWrite = new JavaPatternOclassWriteMethod(this);
	}


	public TmaxStatement tmaxStatement = new TmaxStatement();

	public OmaxStatement makeFromFile(String filePath, String folderPath) throws IOException, DefectLetter, DefectPattern, DefectOffset, DefectFile, DefectGenerator, DefectLimit {
		var fp = Paths.get(filePath);
		var sourceByte = Files.readAllBytes(fp);
		var sourceString = new String(sourceByte);
		var ret = makeFromString(sourceString, folderPath);
		return ret;
	}

	public OmaxStatement makeFromString(String sourceString, String folderPath) throws DefectOffset, DefectPattern, DefectLetter, IOException, DefectFile, DefectGenerator, DefectLimit {
		var sourceText = new StringText(sourceString);
		var ret = makeFromText(sourceText, folderPath);
		return ret;
	}

	public OmaxStatement makeFromText(Text sourceText, String folderPath) throws DefectLetter, DefectOffset, DefectPattern, DefectFile, IOException, DefectGenerator, DefectLimit {
		var data = new Data();
		data.text = sourceText;
		var omaxStatement = tmaxStatement.transport(data, 0);
		System.out.println("");
		checkUsedPatterns(omaxStatement, data);
		System.out.println("");
		for (var stat : omaxStatement.statementList) {
			stat.iClass = makeIclass(stat, omaxStatement, data);
			stat.oClass = makeOclass(stat, omaxStatement, data);
		}
		writeFiles(omaxStatement, data, folderPath);
		System.out.println("");
		checkNotUsedPatterns(omaxStatement, data);
		return omaxStatement;
	}

	public void writeFiles(OmaxStatement omaxStatement, Data data, String folderPath) throws DefectFile, IOException {
		if (folderPath == null) {
			System.out.println("No files are written because folderPath == null.");
			return;
		}
		if (!Files.exists(Paths.get(folderPath))) {
			Files.createDirectories(Paths.get(folderPath));
		}
		// DataCompiler file.
		var dataFilePath = Paths.get(folderPath + "/" + makeDataClassName() + ".java");
		if (!Files.exists(dataFilePath)) {
			Files.createFile(dataFilePath);
		}
		Files.writeString(dataFilePath, makeDataClass(omaxStatement, data));
		System.out.println("Data file written: " + dataFilePath);
		// Idea file.
		var ideaFilePath = Paths.get(folderPath + "/" + makeIdeaClassName() + ".java");
		if (!Files.exists(ideaFilePath)) {
			Files.createFile(ideaFilePath);
		}
		Files.writeString(ideaFilePath, makeIdeaClass(omaxStatement));
		System.out.println("Idea file written: " + ideaFilePath);
		// Iclass files.
		var iClassFolderPath = Paths.get(folderPath + "/" + iClassFolderName);
		deleteFolder(iClassFolderPath);
		Files.createDirectories(iClassFolderPath);
		for (var stat : omaxStatement.statementList) {
			var identifier = stat.name;
			var iClassFilePath = Paths.get(iClassFolderPath + "/" + makeIclassName(identifier) + ".java");
			if (!Files.exists(iClassFilePath)) {
				Files.createFile(iClassFilePath);
			}
			Files.writeString(iClassFilePath, stat.iClass);
//			System.out.println("Iclass file written: " + iClassFilePath);
		}
		// Oclass files.
		for (var stat : omaxStatement.statementList) {
			var identifier = stat.name;
			var oClassFilePath = Paths.get(folderPath + "/" + makeOclassName(identifier) + ".java");
			if (!Files.exists(oClassFilePath)) {
				Files.createFile(oClassFilePath);
				Files.writeString(oClassFilePath, stat.oClass);
				System.out.println("Oclass file written: " + oClassFilePath);
			} else if (overrideOclassFile) {
				Files.writeString(oClassFilePath, stat.oClass);
				System.out.println("Oclass file written: " + oClassFilePath);
			}
		}
	}

	public void deleteFolder(Path path) throws IOException {
		if (!Files.exists(path)) {
			return;
		}
		Files.walk(path).forEach(p -> {
//			System.out.println(p);
			try {
				if (Files.isDirectory(p)) {
					if (!p.equals(path)) {
						deleteFolder(p);
					}
				} else {
					Files.delete(p);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
	}

	public String makeIdeaClass(OmaxStatement oMaxStatement) {
		var nameSet = oMaxStatement.statementMap.keySet();
		var nameList = new ArrayList();
		nameList.addAll(nameSet);
		Collections.sort(nameList);
		var ret = "";
		ret += makeLine(0, "package " + makeOclassPackage() + ";");
		ret += "\n";
		ret += "public enum " + makeIdeaClassName() + " {\n";
		for (var name : nameList) {
			ret += makeLine(1, name + ",");
		}
		ret += "}\n";
		return ret;
	}


	public void checkNotUsedPatterns(OmaxStatement oMaxStatement, Data data) {
		var unusedNameSet = new HashSet<String>();
		unusedNameSet.addAll(oMaxStatement.statementMap.keySet());
		for (var stat : oMaxStatement.statementList) {
			if (stat.oDef.defList != null) {
				for (var odef : stat.oDef.defList) {
					unusedNameSet.remove(odef.name);
				}
			}
		}
		for (var set : data.usedNameMap.values()) {
			unusedNameSet.removeAll(set);
		}
		for (var name : unusedNameSet) {
			System.out.println("Note: Pattern not used: \"" + name + "\"");
		}
	}

	/**
	 *
	 */
	public void checkUsedPatterns(OmaxStatement oMaxStatement, Data data) throws DefectPattern {
		for (var stat : oMaxStatement.statementList) {
			if (stat.oDef.defList != null) {
				for (var odef : stat.oDef.defList) {
					if (!oMaxStatement.statementMap.containsKey(odef.name)) {
						throw new DefectPattern("Defect pattern: " + stat.name + ". Pattern \"" + odef.name + "\" is used but not defined.");
					}
				}
			}
		}
		for (var name : data.usedNameMap.keySet()) {
			var set = data.usedNameMap.get(name);
			for (var usedName : set) {
				if (!oMaxStatement.statementMap.containsKey(usedName)) {
					throw new DefectPattern("Defect pattern: " + name + ". Pattern " + usedName + " is used but not defined.");
				}
			}
		}
	}


	public String makeLine(int indent, String line) {
		var ret = "";
		for (var i = 0; i < indent; i++) {
			ret += "\t";
		}
		return ret + line + "\n";
	}


	public String makeIclass(Ostatement stat, OmaxStatement omaxStatement, Data data) throws DefectOffset, DefectLetter, DefectPattern, DefectGenerator {
		var ret = makeIclassHeader(stat, omaxStatement, data);
		ret += "\n";
		ret += makeIclassBegin(stat, omaxStatement, data);
		ret += "\n";
		ret += makeIclassEnd(stat, omaxStatement, data);
		return ret;
	}

	public String makeOclass(Ostatement stat, OmaxStatement omaxStatement, Data data) throws DefectOffset, DefectLetter, DefectPattern, DefectGenerator {
		var ret = makeOclassHeader(stat, omaxStatement, data);
		ret += "\n";
		ret += makeOclassBegin(stat, omaxStatement, data);
		ret += "\n";
		ret += makeOclassEnd(stat, omaxStatement, data);
		return ret;
	}

	public String makeIclassPackage() {
		return makeOclassPackage() + ".iclass";
	}

	public String makeOclassPackage() {
		return "";
	}

	public String makeIclassBegin(Ostatement stat, OmaxStatement omaxStatement, Data data) throws DefectOffset {
		var ret = "";
		var patternName = (stat.name);
		var statementString = data.text.getFromTo(stat.begin, stat.end);
		ret += "/**\n";
		ret += " * " + statementString + "\n";
		ret += " */\n";
		ret += "public class " + makeIclassName(patternName);
		var transporterBaseClassName = makeIBaseClassName(patternName, data);
		if (transporterBaseClassName != null) {
			ret += " : " + transporterBaseClassName + "()";
		}
		ret += " {\n";
		if (stat.oDef.keyword == data.keywordText) {
			ret += "\n";
			var str = JavaString.addEscapeSequences(stat.oDef.str);
			ret += makeLine(1, "public static Text text = new StringText(\"" + str + "\");");
		}
		return ret;
	}

	public String makeOclassBegin(Ostatement stat, OmaxStatement omaxStatement, Data data) throws DefectOffset {
		var ret = "";
		var patternName = (stat.name);
		var statementString = data.text.getFromTo(stat.begin, stat.end);
		ret += "/**\n";
		ret += " * " + statementString + "\n";
		ret += " */\n";
		ret += "public class " + makeOclassName(patternName);
		var transporterBaseClassName = makeOBaseClassName(patternName, data);
		if (transporterBaseClassName != null) {
			ret += " : " + transporterBaseClassName + "()";
		}
		ret += " {\n\n";
		ret += makeLine(1, "public static final " + makeIdeaClassName() + " idea = " + makeIdeaClassName() + "." + patternName + ";");
		return ret;
	}

	public String makeIclassToStringMethod(Ostatement stat, OmaxStatement omaxStatement, Data data) throws DefectOffset {
		var ret = "";
		ret += makeLine(1, "public String toString() {");
		var statementString = data.text.getFromTo(stat.begin, stat.end).toString();
		ret += makeLine(1, "	return \"" + JavaString.addEscapeSequences(statementString) + "\";");
		ret += makeLine(1, "}");
		return ret;
	}

	public String makeIclassName(String name) {
		return "I" + name;
	}

	public String makeOclassName(String name) {
		return "O" + name;
	}

	public String makeOvalueName(String name) {
		return "o" + name;
	}

	public String makeIBaseClassName(String name, Data data) {
		return null;
	}

	public String makeOBaseClassName(String name, Data data) {
		return null;
	}

	public String makeIdeaClassName() {
		return "Idea";
	}

	public String makeDataClassName() {
		return "Data";
	}

	public String makeDataClass(OmaxStatement omaxStatement, Data data) {
		var ret = "";
		ret += makeLine(0, "package " + makeOclassPackage() + ";");
		ret += "\n";
		ret += makeDataClassHeader(omaxStatement, data);
		ret += "\n";
		ret += makeDataClassBegin(omaxStatement, data);
		ret += "\n";
		ret += makeDataClassEnd(omaxStatement, data);
		return ret;
	}


	public String makeDataClassBegin(OmaxStatement omaxStatement, Data data) {
		var ret = "";
		ret += "public class " + makeDataClassName() + " {\n";
		return ret;
	}

	public String makeDataClassHeader(OmaxStatement omaxStatement, Data data) {
		var ret = "";
		ret += makeLine(0, "import com.gitlab.gerhard_piette.defect_2.DefectOffset;");
		ret += makeLine(0, "import com.gitlab.gerhard_piette.letter_1.DefectLetter;");
		ret += makeLine(0, "import com.gitlab.gerhard_piette.letter_1.LetterEnd;");
		ret += makeLine(0, "import com.gitlab.gerhard_piette.text_6.Text;");
		return ret;
	}

	public String makeDataClassEnd(OmaxStatement omaxStatement, Data data) {
		var ret = "";
		ret += makeLine(1, "public Text text;");
		ret += makeLine(0, "");
		ret += makeLine(1, "public LetterEnd read(int offset) throws DefectLetter, DefectOffset {");
		ret += makeLine(1, "	var ret = text.read(offset);");
		ret += makeLine(1, "	return ret;");
		ret += makeLine(1, "}");
		ret += makeLine(0, "");
		for (var stat : omaxStatement.statementList) {
			ret += makeLine(1, "public " + makeOclassName(stat.name) + " " + makeOvalueName(stat.name) + " = null;");
			ret += makeLine(0, "");
		}
		ret += makeLine(0, "}");
		return ret;
	}

	public String makeIclassHeader(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		ret += makeLine(0, "package " + makeIclassPackage() + ";");
		ret += "\n";
		ret += makeLine(0, "import " + makeOclassPackage() + "." + makeOclassName(stat.name) + ";");
		ret += makeLine(0, "import " + makeOclassPackage() + "." + makeDataClassName() + ";");
		ret += makeLine(0, "import com.gitlab.gerhard_piette.defect_2.DefectOffset;");
		ret += makeLine(0, "import com.gitlab.gerhard_piette.java_string_1.JavaString;");
		ret += makeLine(0, "import com.gitlab.gerhard_piette.letter_1.DefectLetter;");
		ret += makeLine(0, "import com.gitlab.gerhard_piette.text_6.Offset;");
		ret += makeLine(0, "import com.gitlab.gerhard_piette.text_6.StringText;");
		ret += makeLine(0, "import com.gitlab.gerhard_piette.text_6.Text;");
		ret += makeLine(0, "import java.util.List;");
		ret += makeLine(0, "import java.util.ArrayList;");
		ret += makeLine(0, "import java.util.List;");
		ret += makeLine(0, "");
		return ret;
	}

	public String makeOclassHeader(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		ret += makeLine(0, "package " + makeOclassPackage() + ";");
		ret += "\n";
		ret += makeLine(0, "import com.gitlab.gerhard_piette.defect_2.DefectOffset;");
		ret += makeLine(0, "import com.gitlab.gerhard_piette.java_string_1.JavaString;");
		ret += makeLine(0, "import com.gitlab.gerhard_piette.letter_1.DefectLetter;");
		ret += makeLine(0, "import com.gitlab.gerhard_piette.text_6.Offset;");
		ret += makeLine(0, "import com.gitlab.gerhard_piette.text_6.StringText;");
		ret += makeLine(0, "import com.gitlab.gerhard_piette.text_6.Text;");
		ret += makeLine(0, "import java.util.ArrayList;");
		ret += makeLine(0, "import java.util.List;");
		ret += makeLine(0, "");
		return ret;
	}

	public String makeIclassEnd(Ostatement stat, OmaxStatement omaxStatement, Data data) throws DefectGenerator, DefectOffset, DefectPattern {
		var ret = "";
		ret += makeIclassReadMethod(stat, omaxStatement, data);
		ret += makeLine(0, "");
		ret += makeIclassToStringMethod(stat, omaxStatement, data);
		ret += makeLine(0, "");
		ret += "}\n";
		return ret;
	}


	public String makeOclassEnd(Ostatement stat, OmaxStatement omaxStatement, Data data) throws DefectGenerator, DefectOffset, DefectPattern {
		var ret = "";
		ret += makeLine(1, "public int begin = -1;");
		ret += makeLine(0, "");
		ret += makeLine(1, "public int end = -1;");
		ret += makeLine(0, "");
		ret += makeOclassStandardFields(stat, omaxStatement, data);
		ret += makeLine(0, "");
		ret += makeOclassOtherFields(stat, omaxStatement, data);
		ret += makeLine(0, "");
		ret += makeOclassWriteMethod(stat, omaxStatement, data);
		ret += makeLine(0, "");
		ret += makeOclassToStringMethod(stat, omaxStatement, data);
		ret += makeLine(0, "");
		ret += "}\n";
		return ret;
	}

	public String makeOclassStandardFields(Ostatement stat, OmaxStatement omaxStatement, Data data) throws DefectGenerator {
		var ret = jpFields.makeFields(stat, omaxStatement, data);
		return ret;
	}

	public String makeOclassOtherFields(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		return "";
	}

	public String makeOclassWriteMethod(Ostatement stat, OmaxStatement omaxStatement, Data data) throws DefectGenerator, DefectOffset {
		var ret = jpWrite.makeWriteMethod(stat, omaxStatement, data);
		return ret;
	}

	public String makeIclassReadMethod(Ostatement stat, OmaxStatement omaxStatement, Data data) throws DefectGenerator, DefectOffset, DefectPattern {
		var ret = jpRead.makeReadMethod(stat, omaxStatement, data);
		return ret;
	}

	public String makeOclassToStringMethod(Ostatement stat, OmaxStatement omaxStatement, Data data) throws DefectGenerator {
		var ret = jpToString.makeToStringMethod(stat, omaxStatement, data);
		return ret;
	}


	public void setLocalName(List<Odef> defList) {
		//Each output class field must have a unique name. It is possible to declare the same pattern multiple times in a "all" pattern.
		//The remainingSet is used to know what original name can be used. Generated names should not replace original names.
		var remainingNameSet = new HashSet<String>();
		var newNameSet = new HashSet<String>();
		for (var odef : defList) {
			var name = odef.name;
			remainingNameSet.add(name);
			newNameSet.add(name);
		}
		for (var odef : defList) {
			var name = odef.name;
			if (remainingNameSet.contains(name)) {
				odef.localName = name;
				remainingNameSet.remove(name);
			} else {
				var count = 2;
				var newName = name + "_" + count;
				while (newNameSet.contains(newName)) {
					newName = name + "_" + count + "_";
					count++;
				}
				odef.localName = newName;
				newNameSet.add(newName);
			}
		}
	}

}
