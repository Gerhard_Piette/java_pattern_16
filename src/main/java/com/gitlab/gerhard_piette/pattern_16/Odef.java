package com.gitlab.gerhard_piette.pattern_16;

import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.text_6.Text;

import java.util.List;

public class Odef {

	/**
	 * The name of the pattern.
	 */
	public String name;

	/**
	 * A local name for this pattern.
	 */
	public String localName;

	public Text keyword;

	public boolean match = false;

	public int begin = -1;

	public int end = -1;

	public int min = 0;

	public int max = Integer.MAX_VALUE;

	/**
	 * List of used pattern definitions.
	 */
	public List<Odef> defList;

	public Oletter letter;

	public Oletter letter2;

	/**
	 * Text to match.
	 * The text is without the text limits " or """.
	 */
	public String str;


	public String toString() {
		var ret = "";
		ret += "begin:" + begin;
		ret += " end:" + end;
		return ret;
	}


	public String toString(Text text) throws DefectOffset {
		var ret = "";
		ret += "begin:" + begin;
		ret += " end:" + end;
		ret += " text:" + text.getFromTo(begin, end) + "";
		return ret;
	}

}
