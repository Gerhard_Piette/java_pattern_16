package com.gitlab.gerhard_piette.pattern_16;

import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;

import java.util.ArrayList;

public class TdefCheck {

	public static Odef transport(String name, Data dt, int offset) throws DefectLetter, DefectOffset, DefectPattern, DefectLimit {
		var ret = new Odef();
		ret.keyword = dt.keywordCheck;
		ret.begin = offset;
		var off = offset;
		off = dt.afterLeft(off);
		if (off < 0) {
			ret.match = false;
			return ret;
		}
		off = dt.afterSpace(off);
		off = dt.afterMatch(off, dt.keywordCheck);
		if (off < 0) {
			ret.match = false;
			return ret;
		}
		off = dt.afterSpace(off);
		var odefList = new ArrayList<Odef>();
		var odef = TdefLocal.transport(name, dt, off);
		if (!odef.match) {
			var ms = "Defect pattern: " + name + ". Expected pattern after offset " + off + ".";
			throw new DefectPattern(ms);
		}
		odefList.add(odef);
		off = odef.end;
		off = dt.afterSpace(off);
		off = dt.afterRight(off);
		if (off < 0) {
			var ms = "Defect pattern: " + name + ". Expected closing right parenthesis after offset " + off + ".";
			throw new DefectPattern(ms);
		}
		ret.match = true;
		ret.end = off;
		ret.defList = odefList;
		return ret;
	}

}