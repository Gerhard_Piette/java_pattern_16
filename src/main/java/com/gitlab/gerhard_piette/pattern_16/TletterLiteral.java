package com.gitlab.gerhard_piette.pattern_16;

import com.gitlab.gerhard_piette.ascii_1.Ascii;
import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.letter_1.LetterEnd;
import com.gitlab.gerhard_piette.text_6.Text;

public class TletterLiteral {

	/**
	 * https://en.wikipedia.org/wiki/Escape_sequences_in_C
	 * And the \s literal for space.
	 *
	 * @return LetterEnd.
	 */
	public static Oletter transport(String name, Data dt, int offset) throws DefectLetter, DefectOffset, DefectPattern, DefectLimit {
		var ret = new Oletter();
		var off = offset;
//		if (off >= dt.getLength()) {
//			ret.match = false;
//			return ret;
//		}
		var beginLe = dt.read(off);
		if (beginLe.letter != dt.Apostrophe) {
			ret.match = false;
			return ret;
		}
		off = beginLe.end;
		if (off >= dt.getLength()) {
			var ms = "Defect pattern: " + name + ". End of text after offset " + off + ".";
			throw new DefectPattern(ms);
		}
		var letterLe = dt.read(off);
		if (letterLe.letter == Ascii.Reverse_Solidus) {
			letterLe = transportEscapeSequence(name, dt.text, off);
			ret.number = letterLe.letter;
		} else {
			ret.number = letterLe.letter;
		}
		off = letterLe.end;
		if (off >= dt.getLength()) {
			var ms = "Defect pattern: " + name + ". No closing apostrophe declared. End of text after offset " + off + ".";
			throw new DefectPattern(ms);
		}
		var endLe = dt.read(off);
		if (endLe.letter != Ascii.Apostrophe) {
			var ms = "Defect pattern: " + name + ". No closing apostrophe declared. Lack of closing apostrophe after offset " + off + ".";
			throw new DefectPattern(ms);
		}
		off = endLe.end;
		dt.checkLimit(off);
		ret.end = off;
		ret.match = true;
		return ret;
	}

	/**
	 * https://en.wikipedia.org/wiki/Escape_sequences_in_C
	 * And the \s literal for space.
	 *
	 * @return LetterEnd. The number is the number of the expressed letter. The end is after the escape sequence.
	 */
	public static LetterEnd transportEscapeSequence(String name, Text text, int offset) throws DefectLetter, DefectOffset, DefectPattern {
		var leftDiv = text.read(offset);
		if (leftDiv.letter != Ascii.Reverse_Solidus) {
			var ms = "No valid escape sequence in pattern '" + name + "' after offset: " + offset + ".";
			throw new DefectPattern(ms);
		}
		var le = text.read(leftDiv.end);
		switch (le.letter) {
			case Ascii.Small_A:
				return new LetterEnd(Ascii.Bell, le.end);
			case Ascii.Small_B:
				return new LetterEnd(Ascii.Backspace, le.end);
			case Ascii.Small_E:
				return new LetterEnd(Ascii.Escape, le.end);
			case Ascii.Small_F:
				return new LetterEnd(Ascii.Form_feed, le.end);
			case Ascii.Small_N:
				return new LetterEnd(Ascii.Line_feed, le.end);
			case Ascii.Small_R:
				return new LetterEnd(Ascii.Capital_R, le.end);
			case Ascii.Small_S:
				return new LetterEnd(Ascii.Space, le.end);
			case Ascii.Small_T:
				return new LetterEnd(Ascii.Horizontal_Tab, le.end);
			case Ascii.Small_V:
				return new LetterEnd(Ascii.Vertical_Tab, le.end);
			case Ascii.Apostrophe:
				return new LetterEnd(Ascii.Apostrophe, le.end);
		}
		var ms = "No valid escape sequence in pattern '" + name + "' after offset: " + leftDiv.end + ". Sequence: " + text.getFromTo(offset, le.end);
		throw new DefectPattern(ms);
	}


}
