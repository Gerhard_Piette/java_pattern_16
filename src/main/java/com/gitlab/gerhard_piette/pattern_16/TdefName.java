package com.gitlab.gerhard_piette.pattern_16;

import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;

/**
 * To match the name of a pattern that is used in the definition of a pattern.
 * ATTENTION: Must be used after the defCopy
 */
public class TdefName {

	public static Odef transport(String name, Data dt, int offset) throws DefectLetter, DefectOffset, DefectPattern, DefectLimit {
		var ret = new Odef();
		ret.keyword = dt.keywordName;
		ret.begin = offset;
		var oname = Tname.transport(name, dt, offset);
		ret.name = oname.name;
		if (!oname.match) {
			ret.match = false;
			return ret;
		}
		ret.end = oname.end;
		ret.match = true;
		return ret;
	}

}