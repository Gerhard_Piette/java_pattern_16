package com.gitlab.gerhard_piette.pattern_16;

public class DefectFile extends Exception {

	public DefectFile(String message) {
		super(message);
	}
}
