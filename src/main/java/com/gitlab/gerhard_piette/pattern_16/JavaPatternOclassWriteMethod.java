package com.gitlab.gerhard_piette.pattern_16;

import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.text_6.Text;


public class JavaPatternOclassWriteMethod {

	public JavaPattern jp;

	public JavaPatternOclassWriteMethod(JavaPattern jp) {
		this.jp = jp;
	}

	public String makeLine(int indentLevel, String line) {
		return jp.makeLine(indentLevel, line);
	}

	public String makeOvalueName(String name) {
		return jp.makeOvalueName(name);
	}

	public String makeWriteMethod(Ostatement stat, OmaxStatement omaxStatement, Data data) throws DefectGenerator, DefectOffset {
		var ret = "";
		var statementString = data.text.getFromTo(stat.begin, stat.end);
		ret += makeLine(1, "/**");
		ret += makeLine(1, " * " + statementString);
		ret += makeLine(1, " */");
		ret += makeLine(1, "public String write(Data dt) throws DefectOffset {");
		ret += makeLogic(stat, omaxStatement, data);
		ret += makeLine(1, "}");
		return ret;
	}

	public String makeLogic(Ostatement stat, OmaxStatement omaxStatement, Data data) throws DefectGenerator {
		Text keyword = stat.oDef.keyword;
		if (keyword == data.keywordAll) {
			return makeAll(stat, omaxStatement, data);
		}
		if (keyword == data.keywordCheck) {
			return makeCheck(stat, omaxStatement, data);
		}
		if (keyword == data.keywordCopy) {
			return makeCopy(stat, omaxStatement, data);
		}
		if (keyword == data.keywordEnd) {
			return makeEnd(stat, omaxStatement, data);
		}
		if (keyword == data.keywordMax) {
			return makeMax(stat, omaxStatement, data);
		}
		if (keyword == data.keywordMaxto) {
			return makeMaxTo(stat, omaxStatement, data);
		}
		if (keyword == data.keywordMaybe) {
			return makeMaybe(stat, omaxStatement, data);
		}
		if (keyword == data.keywordMin) {
			return makeMin(stat, omaxStatement, data);
		}
		if (keyword == data.keywordMinto) {
			return makeMinTo(stat, omaxStatement, data);
		}
		if (keyword == data.keywordNot) {
			return makeNot(stat, omaxStatement, data);
		}
		if (keyword == data.keywordOne) {
			return makeOne(stat, omaxStatement, data);
		}
		if (keyword == data.keywordText) {
			return makeText(stat, omaxStatement, data);
		}
		if (keyword == data.keywordEqual) {
			return makeEqual(stat, omaxStatement, data);
		}
		if (keyword == data.keywordUnequal) {
			return makeUnequal(stat, omaxStatement, data);
		}
		if (keyword == data.keywordPointyLeft) {
			return makePointyLeft(stat, omaxStatement, data);
		}
		if (keyword == data.keywordPointyRight) {
			return makePointyRight(stat, omaxStatement, data);
		}
		if (keyword == data.keywordPointyLeftEqual) {
			return makePointyLeftEqual(stat, omaxStatement, data);
		}
		if (keyword == data.keywordPointyRightEqual) {
			return makePointyRightEqual(stat, omaxStatement, data);
		}
		if (keyword == data.keywordPointy1) {
			return makePointy1(stat, omaxStatement, data);
		}
		if (keyword == data.keywordPointy2) {
			return makePointy2(stat, omaxStatement, data);
		}
		if (keyword == data.keywordPointyEqual1) {
			return makePointyEqual1(stat, omaxStatement, data);
		}
		if (keyword == data.keywordPointyEqual2) {
			return makePointyEqual2(stat, omaxStatement, data);
		}
		var ms = "Keyword not supported: " + keyword + ".";
		throw new DefectGenerator(ms);
	}

	public String makeAll(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		ret += makeLine(2, "var ret = \"\";");
		jp.setLocalName(stat.oDef.defList);
		for (var odef : stat.oDef.defList) {
			var name = odef.name;
			ret += makeLine(2, "if (" + makeOvalueName(odef.localName) + ".end >= 0) {");
			ret += makeLine(2, "	ret += " + makeOvalueName(odef.localName) + ".write(dt);");
			ret += makeLine(2, "}");
		}
		ret += makeLine(2, "return ret;");
		return ret;
	}

	public String makeCheck(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = makeLine(2, "return \"\";");
		return ret;
	}

	public String makeCopy(Ostatement stat, OmaxStatement omaxStatement, Data data) throws DefectGenerator {
//		var name = stat.oDef.defList.get(0).name;
//		var stat2 = omaxStatement.statementMap.get(name);
//		var ret = makeLogic(stat2, omaxStatement, data);
//		return ret;
		var ret = "";
		ret += makeLine(2, "var ret = \"\";");
		var name = stat.oDef.defList.get(0).name;
		ret += makeLine(2, "if (" + makeOvalueName(name) + ".end >= 0) {");
		ret += makeLine(2, "	ret += " + makeOvalueName(name) + ".write(dt);");
		ret += makeLine(2, "}");
		ret += makeLine(2, "return ret;");
		return ret;
	}

	public String makeEnd(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = makeLine(2, "return \"\";");
		return ret;
	}

	public String makeMax(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		ret += makeLine(2, "var ret = \"\";");
		var name = stat.oDef.defList.get(0).name;
		ret += makeLine(2, "for (var " + makeOvalueName(name) + " : " + makeOvalueName(name) + "List) {");
		ret += makeLine(2, "	ret += " + makeOvalueName(name) + ".write(dt);");
		ret += makeLine(2, "}");
		ret += makeLine(2, "return ret;");
		return ret;
	}

	public String makeMaxTo(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		ret += makeLine(2, "var ret = \"\";");
		var name = stat.oDef.defList.get(0).name;
		var name2 = stat.oDef.defList.get(1).name;
		ret += makeLine(2, "for (var " + makeOvalueName(name) + " : " + makeOvalueName(name) + "List) {");
		ret += makeLine(2, "	ret += " + makeOvalueName(name) + ".write(dt);");
		ret += makeLine(2, "}");
		ret += makeLine(2, "ret += " + makeOvalueName(name2) + ".write(dt);");
		ret += makeLine(2, "return ret;");
		return ret;
	}

	public String makeMaybe(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		var name = stat.oDef.defList.get(0).name;
		ret += makeLine(2, "var ret = " + makeOvalueName(name) + ".write(dt);");
		ret += makeLine(2, "return ret;");
		return ret;
	}

	public String makeMin(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		ret += makeLine(2, "var ret = \"\";");
		var name = stat.oDef.defList.get(0).name;
		ret += makeLine(2, "for (var " + makeOvalueName(name) + " : " + makeOvalueName(name) + "List) {");
		ret += makeLine(2, "	ret += " + makeOvalueName(name) + ".write(dt);");
		ret += makeLine(2, "}");
		ret += makeLine(2, "return ret;");
		return ret;
	}

	public String makeMinTo(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		ret += makeLine(2, "var ret = \"\";");
		var name = stat.oDef.defList.get(0).name;
		var name2 = stat.oDef.defList.get(1).name;
		ret += makeLine(2, "for (var " + makeOvalueName(name) + " : " + makeOvalueName(name) + "List) {");
		ret += makeLine(2, "	ret += " + makeOvalueName(name) + ".write(dt);");
		ret += makeLine(2, "}");
		ret += makeLine(2, "ret += " + makeOvalueName(name2) + ".write(dt);");
		ret += makeLine(2, "return ret;");
		return ret;
	}

	public String makeNot(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = makeLine(2, "return \"\";");
		return ret;
	}

	public String makeOne(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		for (var odef : stat.oDef.defList) {
			ret += makeLine(2, "if (" + makeOvalueName(odef.name) + ".end >= 0) {");
			ret += makeLine(2, "	var ret = " + makeOvalueName(odef.name) + ".write(dt);");
			ret += makeLine(2, "	return ret;");
			ret += makeLine(2, "}");
		}
		ret += makeLine(2, "return \"\";");
		return ret;
	}

	public String makeText(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = makeLine(2, "return dt.text.getFromTo(begin, end).toString();");
		return ret;
	}


	public String makeEqual(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = makeLine(2, "return dt.text.getFromTo(begin, end).toString();");
		return ret;
	}

	public String makeUnequal(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = makeLine(2, "return dt.text.getFromTo(begin, end).toString();");
		return ret;
	}

	public String makePointyLeft(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = makeLine(2, "return dt.text.getFromTo(begin, end).toString();");
		return ret;
	}

	public String makePointyRight(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = makeLine(2, "return dt.text.getFromTo(begin, end).toString();");
		return ret;
	}

	public String makePointyLeftEqual(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = makeLine(2, "return dt.text.getFromTo(begin, end).toString();");
		return ret;
	}

	public String makePointyRightEqual(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = makeLine(2, "return dt.text.getFromTo(begin, end).toString();");
		return ret;
	}

	public String makePointy1(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = makeLine(2, "return dt.text.getFromTo(begin, end).toString();");
		return ret;
	}

	public String makePointy2(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = makeLine(2, "return dt.text.getFromTo(begin, end).toString();");
		return ret;
	}

	public String makePointyEqual1(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = makeLine(2, "return dt.text.getFromTo(begin, end).toString();");
		return ret;
	}

	public String makePointyEqual2(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = makeLine(2, "return dt.text.getFromTo(begin, end).toString();");
		return ret;
	}
}
