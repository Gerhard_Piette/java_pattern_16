package com.gitlab.gerhard_piette.pattern_16;

import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;

import java.util.ArrayList;

public class TdefCopy {

	public static Odef transport(String name, Data dt, int offset) throws DefectLetter, DefectOffset, DefectPattern, DefectLimit {
		var ret = new Odef();
		ret.keyword = dt.keywordCopy;
		ret.begin = offset;
		var oname = TdefName.transport(name, dt, offset);
		if (!oname.match) {
			ret.match = false;
			return ret;
		}
		var list = new ArrayList<Odef>();
		list.add(oname);
		ret.defList = list;
		ret.end = oname.end;
		ret.match = true;
		return ret;
	}

}