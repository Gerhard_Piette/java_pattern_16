package com.gitlab.gerhard_piette.pattern_16;

import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.text_6.Offset;

public class Tcomment {

	public static Odef transport(Data dt, int offset) throws DefectLetter, DefectOffset {
		var ret = new Odef();
		ret.keyword = dt.keywordComment;
		ret.begin = offset;
		var off = offset;
		off = Offset.afterMatch(dt.text, off, dt.keywordComment, 0, dt.keywordComment.getLength());
		if (off < 0) {
			ret.match = false;
			return ret;
		}
		while (off < dt.getLength()) {
			var le = dt.read(off);
			off = le.end;
			if (le.letter == dt.LinEnd) {
				break;
			}
		}
		ret.end = off;
		ret.match = true;
		return ret;
	}

}