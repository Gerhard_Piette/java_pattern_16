package com.gitlab.gerhard_piette.pattern_16;

import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;

public class Tname {

	public static Oname transport(String name, Data dt, int offset) throws DefectLetter, DefectOffset, DefectPattern, DefectLimit {
		var ret = new Oname();
		var off = offset;
		var le = dt.read(off);
		//A name can not begin with a number.
		if (!dt.isIdentifierFirstLetter(le.letter)) {
			ret.match = false;
			return ret;
		}
		off = le.end;
		while (off < dt.getLength()) {
			le = dt.read(off);
			if (dt.isIdentifierOtherLetter(le.letter)) {
				off = le.end;
			} else {
				break;
			}
		}
		//The letter of the referenced pattern is resolved in TmaxStatement.resolveLetterName.
		ret.name = dt.getText(offset, off).toString();
		dt.checkLimit(off);
		ret.end = off;
		ret.match = true;
		return ret;
	}

}
