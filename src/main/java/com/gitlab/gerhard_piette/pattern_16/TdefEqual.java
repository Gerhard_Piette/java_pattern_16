package com.gitlab.gerhard_piette.pattern_16;

import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;

public class TdefEqual {

	public static Odef transport(String name, Data dt, int offset) throws DefectLetter, DefectOffset, DefectPattern, DefectLimit {
		var ret = new Odef();
		ret.keyword = dt.keywordEqual;
		ret.begin = offset;
		var off = offset;
		off = dt.afterMatch(off, dt.keywordEqual);
		if (off < 0) {
			ret.match = false;
			return ret;
		}
		off = dt.afterSpace(off);
		var oletter = Tletter.transport(name, dt, off);
		if (oletter.match) {
			ret.letter = oletter;
		} else {
			var ms = "Defect pattern: " + name + ". Lack of letter declaration after offset " + off + ".";
			throw new DefectPattern(ms);
		}
		off = oletter.end;
		ret.end = off;
		ret.match = true;
		return ret;
	}

}