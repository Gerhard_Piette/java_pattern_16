package com.gitlab.gerhard_piette.pattern_16;

import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;

public class TdefCommon {

	public static Odef transport(String name, Data dt, int offset) throws DefectLetter, DefectOffset, DefectPattern, DefectLimit {
		Odef ret = null;
		ret = TdefAll.transport(name, dt, offset);
		if (ret.match) {
			return ret;
		}
		ret = TdefCheck.transport(name, dt, offset);
		if (ret.match) {
			return ret;
		}
//		ret = TdefCopy.transport(name, dt, offset);
//		if (ret.match) {
//			return ret;
//		}
		ret = TdefEnd.transport(name, dt, offset);
		if (ret.match) {
			return ret;
		}
		ret = TdefMaxTo.transport(name, dt, offset);
		if (ret.match) {
			return ret;
		}
		ret = TdefMax.transport(name, dt, offset);
		if (ret.match) {
			return ret;
		}
		ret = TdefMaybe.transport(name, dt, offset);
		if (ret.match) {
			return ret;
		}
		ret = TdefMinTo.transport(name, dt, offset);
		if (ret.match) {
			return ret;
		}
		ret = TdefMin.transport(name, dt, offset);
		if (ret.match) {
			return ret;
		}
//		ret = TdefName.transport(name, dt, offset);
//		if (ret.match) {
//			return ret;
//		}
		ret = TdefNot.transport(name, dt, offset);
		if (ret.match) {
			return ret;
		}
		ret = TdefOne.transport(name, dt, offset);
		if (ret.match) {
			return ret;
		}
		ret = TdefText.transport(name, dt, offset);
		if (ret.match) {
			return ret;
		}
		ret = TdefEqual.transport(name, dt, offset);
		if (ret.match) {
			return ret;
		}
		ret = TdefUnequal.transport(name, dt, offset);
		if (ret.match) {
			return ret;
		}
		ret = TdefPointyRightEqual.transport(name, dt, offset);
		if (ret.match) {
			return ret;
		}
		ret = TdefPointyRight.transport(name, dt, offset);
		if (ret.match) {
			return ret;
		}
		ret = TdefPointyLeftEqual.transport(name, dt, offset);
		if (ret.match) {
			return ret;
		}
		ret = TdefPointyLeft.transport(name, dt, offset);
		if (ret.match) {
			return ret;
		}
		ret = TdefPointyEqual2.transport(name, dt, offset);
		if (ret.match) {
			return ret;
		}
		ret = TdefPointyEqual1.transport(name, dt, offset);
		if (ret.match) {
			return ret;
		}
		ret = TdefPointy2.transport(name, dt, offset);
		if (ret.match) {
			return ret;
		}
		ret = TdefPointy1.transport(name, dt, offset);
		if (ret.match) {
			return ret;
		}
		return ret;
	}

}