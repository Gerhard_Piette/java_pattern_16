package com.gitlab.gerhard_piette.pattern_16;

import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;

public class Tletter {

	public static Oletter transport(String name, Data dt, int offset) throws DefectLetter, DefectOffset, DefectPattern, DefectLimit {
		Oletter ret = null;
		ret = TletterNumber.transport(name, dt, offset);
		if (ret.match) {
			return ret;
		}
		ret = TletterLiteral.transport(name, dt, offset);
		if (ret.match) {
			return ret;
		}
		ret = TletterName.transport(name, dt, offset);
		return ret;
	}

}
