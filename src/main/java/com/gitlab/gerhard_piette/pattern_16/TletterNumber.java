package com.gitlab.gerhard_piette.pattern_16;

import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.integer_1.UnicodeInteger;

public class TletterNumber {

	public static UnicodeInteger unicodeInteger = new UnicodeInteger();

	public static Oletter transport(String name, Data dt, int offset) throws DefectLetter, DefectOffset, DefectPattern, DefectLimit {
		var ret = new Oletter();
		var off = offset;
//		off = dt.afterMatch(off, dt.keywordU);
//		if (off < 0) {
//			ret.match = false;
//			return ret;
//		}
		var afterInteger = unicodeInteger.afterIntegerDecWithUnderscore(dt.text, off);
		if (afterInteger < 0) {
			ret.match = false;
			return ret;
//			var ms = "Defect pattern: " + name + ". Lack of integer after offset " + off + ".";
//			throw new DefectPattern(ms);
		}
		var str = dt.text.getFromTo(offset, afterInteger).toString();
		str = str.replaceAll("_", "");
		ret.number = Integer.valueOf(str);
		dt.checkLimit(afterInteger);
		ret.end = afterInteger;
		ret.match = true;
		return ret;
	}


	public static String stringOfLetter(int letter) {
		var ret = new StringBuilder();
		ret.appendCodePoint(letter);
		return ret.toString();
	}

}
