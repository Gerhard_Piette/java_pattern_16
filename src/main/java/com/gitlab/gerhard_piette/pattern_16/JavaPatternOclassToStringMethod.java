package com.gitlab.gerhard_piette.pattern_16;

import com.gitlab.gerhard_piette.java_string_1.JavaString;
import com.gitlab.gerhard_piette.text_6.Text;

public class JavaPatternOclassToStringMethod {

	public JavaPattern jp;

	public JavaPatternOclassToStringMethod(JavaPattern jp) {
		this.jp = jp;
	}

	public String makeLine(int indentLevel, String line) {
		return jp.makeLine(indentLevel, line);
	}

	public String makeOvalueName(String name) {
		return jp.makeOvalueName(name);
	}

	public String makeIndentStringPerLevel() {
		return "i + \"" + JavaString.addEscapeSequences("\t") + "\"";
	}

	public String makeToStringMethod(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		ret += makeLine(1, "public String toString(int indentLevel, Data dt) throws DefectOffset {");
		ret += makeLine(1, "	var ret = \"\";");
		ret += makeLine(1, "	for (int i = 0; i < indentLevel; i++) {");
		ret += makeLine(1, "		ret += " + makeIndentStringPerLevel() + ";");
		ret += makeLine(1, "	}");
		ret += makeLine(1, "	ret += this.idea.name();");
		ret += makeLine(1, "	ret += \" begin \" + begin;");
		ret += makeLine(1, "	ret += \" end \" + end;");
		ret += makeLine(1, "	if (end >= begin) {");
		ret += makeLine(1, "		ret += \" text \\\"\" + JavaString.addEscapeSequences(dt.text.getFromTo(begin, end).toString()) + \"\\\"\";");
		ret += makeLine(1, "	}");
		ret += makeLogic(stat, omaxStatement, data);
		ret += makeLine(1, "	return ret;");
		ret += makeLine(1, "}");
		return ret;
	}

	public String makeLogic(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		Text keyword = stat.oDef.keyword;
		if (keyword == data.keywordAll) {
			return makeAll(stat, omaxStatement, data);
		}
		if (keyword == data.keywordCheck) {
			return makeCheck(stat, omaxStatement, data);
		}
		if (keyword == data.keywordCopy) {
			return makeCopy(stat, omaxStatement, data);
		}
		if (keyword == data.keywordEnd) {
			return makeEnd(stat, omaxStatement, data);
		}
		if (keyword == data.keywordMax) {
			return makeMax(stat, omaxStatement, data);
		}
		if (keyword == data.keywordMaxto) {
			return makeMaxTo(stat, omaxStatement, data);
		}
		if (keyword == data.keywordMaybe) {
			return makeMaybe(stat, omaxStatement, data);
		}
		if (keyword == data.keywordMin) {
			return makeMin(stat, omaxStatement, data);
		}
		if (keyword == data.keywordMinto) {
			return makeMinTo(stat, omaxStatement, data);
		}
		if (keyword == data.keywordNot) {
			return makeNot(stat, omaxStatement, data);
		}
		if (keyword == data.keywordOne) {
			return makeOne(stat, omaxStatement, data);
		}
		if (keyword == data.keywordText) {
			return makeText(stat, omaxStatement, data);
		}
		return makeLetter(stat, omaxStatement, data);
	}

	public String makeAll(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		jp.setLocalName(stat.oDef.defList);
		for (var odef : stat.oDef.defList) {
			var localName = odef.localName;
			ret += makeLine(2, "if (" + makeOvalueName(localName) + " != null) {");
			ret += makeLine(2, "	ret += \"\\n\";");
			ret += makeLine(2, "	ret += " + makeOvalueName(localName) + ".toString(indentLevel + 1, dt);");
			ret += makeLine(2, "}");
		}
		return ret;
	}

	public String makeCheck(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		var name = stat.oDef.defList.get(0).name;
		ret += makeLine(2, "if (" + makeOvalueName(name) + " != null) {");
		ret += makeLine(2, "	ret += \"\\n\";");
		ret += makeLine(2, "	ret += " + makeOvalueName(name) + ".toString(indentLevel + 1, dt);");
		ret += makeLine(2, "}");
		return ret;
	}

	public String makeCopy(Ostatement stat, OmaxStatement omaxStatement, Data data) {
//		var ret = "";
//		var name = stat.oDef.defList.get(0).name;
//		var stat2 = omaxStatement.statementMap.get(name);
//		ret += makeLogic(stat2, omaxStatement, data);
//		return ret;
		var ret = "";
		var name = stat.oDef.defList.get(0).name;
		ret += makeLine(2, "if (" + makeOvalueName(name) + " != null) {");
		ret += makeLine(2, "	ret += \"\\n\";");
		ret += makeLine(2, "	ret += " + makeOvalueName(name) + ".toString(indentLevel + 1, dt);");
		ret += makeLine(2, "}");
		return ret;
	}

	public String makeEnd(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		return ret;
	}

	public String makeMax(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		var name = stat.oDef.defList.get(0).name;
		ret += makeLine(2, "for (var x : " + makeOvalueName(name) + "List) {");
		ret += makeLine(2, "	ret += \"\\n\";");
		ret += makeLine(2, "	ret += x.toString(indentLevel + 1, dt);");
		ret += makeLine(2, "}");
		return ret;
	}

	public String makeMaxTo(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		var name = stat.oDef.defList.get(0).name;
		var name2 = stat.oDef.defList.get(1).name;
		ret += makeLine(2, "for (var x : " + makeOvalueName(name) + "List) {");
		ret += makeLine(2, "	ret += \"\\n\";");
		ret += makeLine(2, "	ret += x.toString(indentLevel + 1, dt);");
		ret += makeLine(2, "}");
		ret += makeLine(2, "if (" + makeOvalueName(name2) + " != null) {");
		ret += makeLine(2, "	ret += \"\\n\";");
		ret += makeLine(2, "	ret += " + makeOvalueName(name2) + ".toString(indentLevel + 1, dt);");
		ret += makeLine(2, "}");
		return ret;
	}

	public String makeMaybe(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		var name = stat.oDef.defList.get(0).name;
		ret += makeLine(2, "if (" + makeOvalueName(name) + " != null) {");
		ret += makeLine(2, "	ret += \"\\n\";");
		ret += makeLine(2, "	ret += " + makeOvalueName(name) + ".toString(indentLevel + 1, dt);");
		ret += makeLine(2, "}");
		return ret;
	}

	public String makeMin(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		var name = stat.oDef.defList.get(0).name;
		ret += makeLine(2, "for (var x : " + makeOvalueName(name) + "List) {");
		ret += makeLine(2, "	ret += \"\\n\";");
		ret += makeLine(2, "	ret += x.toString(indentLevel + 1, dt);");
		ret += makeLine(2, "}");
		return ret;
	}

	public String makeMinTo(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		var name = stat.oDef.defList.get(0).name;
		var name2 = stat.oDef.defList.get(1).name;
		ret += makeLine(2, "for (var x : " + makeOvalueName(name) + "List) {");
		ret += makeLine(2, "	ret += \"\\n\";");
		ret += makeLine(2, "	ret += x.toString(indentLevel + 1, dt);");
		ret += makeLine(2, "}");
		ret += makeLine(2, "if (" + makeOvalueName(name2) + " != null) {");
		ret += makeLine(2, "	ret += \"\\n\";");
		ret += makeLine(2, "	ret += " + makeOvalueName(name2) + ".toString(indentLevel + 1, dt);");
		ret += makeLine(2, "}");
		return ret;
	}

	public String makeNot(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		var name = stat.oDef.defList.get(0).name;
		ret += makeLine(2, "if (" + makeOvalueName(name) + " != null) {");
		ret += makeLine(2, "	ret += \"\\n\";");
		ret += makeLine(2, "	ret += " + makeOvalueName(name) + ".toString(indentLevel + 1, dt);");
		ret += makeLine(2, "}");
		return ret;
	}

	public String makeOne(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		for (var odef : stat.oDef.defList) {
			var name = odef.name;
			ret += makeLine(2, "if (" + makeOvalueName(name) + " != null) {");
			ret += makeLine(2, "	ret += \"\\n\";");
			ret += makeLine(2, "	ret += " + makeOvalueName(name) + ".toString(indentLevel + 1, dt);");
			ret += makeLine(2, "}");
		}
		return ret;
	}

	public String makeText(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		return ret;
	}

	public String makeLetter(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		return ret;
	}

}
