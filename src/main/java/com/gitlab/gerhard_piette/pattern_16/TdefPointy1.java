package com.gitlab.gerhard_piette.pattern_16;

import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;

public class TdefPointy1 {

	public static Odef transport(String name, Data dt, int offset) throws DefectLetter, DefectOffset, DefectPattern, DefectLimit {
		var ret = new Odef();
		ret.keyword = dt.keywordPointy1;
		ret.begin = offset;
		var off = offset;
		off = dt.afterLeft(off);
		if (off < 0) {
			ret.match = false;
			return ret;
		}
		//letter 1
		off = dt.afterSpace(off);
		off = dt.afterMatch(off, dt.keywordPointyRight);
		if (off < 0) {
			ret.match = false;
			return ret;
		}
		off = dt.afterSpace(off);
		var oletter = Tletter.transport(name, dt, off);
		if (oletter.match) {
			ret.letter = oletter;
		} else {
			var ms = "Defect pattern: " + name + ". Lack of letter declaration after offset " + off + ".";
			throw new DefectPattern(ms);
		}
		off = oletter.end;
		//letter 2
		off = dt.afterSpace(off);
		off = dt.afterMatch(off, dt.keywordPointyLeft);
		if (off < 0) {
			ret.match = false;
			return ret;
		}
		off = dt.afterSpace(off);
		oletter = Tletter.transport(name, dt, off);
		if (oletter.match) {
			ret.letter2 = oletter;
		} else {
			var ms = "Defect pattern: " + name + ". Lack of letter declaration after offset " + off + ".";
			throw new DefectPattern(ms);
		}
		off = oletter.end;
		off = dt.afterSpace(off);
		off = dt.afterRight(off);
		if (off < 0) {
			var ms = "Defect pattern: " + name + ". Expected closing right parenthesis after offset " + off + ".";
			throw new DefectPattern(ms);
		}
		ret.match = true;
		ret.end = off;
		return ret;
	}

}