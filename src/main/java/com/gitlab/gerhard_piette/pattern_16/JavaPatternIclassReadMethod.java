package com.gitlab.gerhard_piette.pattern_16;

import com.gitlab.gerhard_piette.ascii_1.Ascii;
import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.text_6.Text;


public class JavaPatternIclassReadMethod {

	public JavaPattern jp;

	public JavaPatternIclassReadMethod(JavaPattern jp) {
		this.jp = jp;
	}

	public String makeLine(int indentLevel, String line) {
		return jp.makeLine(indentLevel, line);
	}

	public String makeIclassName(String name) {
		return jp.makeIclassName(name);
	}

	public String makeOclassName(String name) {
		return jp.makeOclassName(name);
	}

	public String makeOvalueName(String name) {
		return jp.makeOvalueName(name);
	}

	public String makeReadMethod(Ostatement stat, OmaxStatement omaxStatement, Data data) throws DefectPattern, DefectGenerator, DefectOffset {
		var ret = "";
		var statementString = data.text.getFromTo(stat.begin, stat.end);
		ret += makeLine(1, "/**");
		ret += makeLine(1, " * " + statementString);
		ret += makeLine(1, " */");
		ret += makeLine(1, "public static int read(Data dt, int offset) throws DefectLetter, DefectOffset {");
		ret += makeLogic(stat, omaxStatement, data);
		ret += makeLine(1, "}");
		return ret;
	}

	public String makeLogic(Ostatement stat, OmaxStatement omaxStatement, Data data) throws DefectPattern, DefectGenerator {
		Text keyword = stat.oDef.keyword;
		if (keyword == data.keywordAll) {
			return makeAll(stat, omaxStatement, data);
		}
		if (keyword == data.keywordCheck) {
			return makeCheck(stat, omaxStatement, data);
		}
		if (keyword == data.keywordCopy) {
			return makeCopy(stat, omaxStatement, data);
		}
		if (keyword == data.keywordEnd) {
			return makeEnd(stat, omaxStatement, data);
		}
		if (keyword == data.keywordMax) {
			return makeMax(stat, omaxStatement, data);
		}
		if (keyword == data.keywordMaxto) {
			return makeMaxTo(stat, omaxStatement, data);
		}
		if (keyword == data.keywordMaybe) {
			return makeMaybe(stat, omaxStatement, data);
		}
		if (keyword == data.keywordMin) {
			return makeMin(stat, omaxStatement, data);
		}
		if (keyword == data.keywordMinto) {
			return makeMinTo(stat, omaxStatement, data);
		}
		if (keyword == data.keywordNot) {
			return makeNot(stat, omaxStatement, data);
		}
		if (keyword == data.keywordOne) {
			return makeOne(stat, omaxStatement, data);
		}
		if (keyword == data.keywordText) {
			return makeText(stat, omaxStatement, data);
		}
		if (keyword == data.keywordEqual) {
			return makeEqual(stat, omaxStatement, data);
		}
		if (keyword == data.keywordUnequal) {
			return makeUnequal(stat, omaxStatement, data);
		}
		if (keyword == data.keywordPointyLeft) {
			return makePointyLeft(stat, omaxStatement, data);
		}
		if (keyword == data.keywordPointyRight) {
			return makePointyRight(stat, omaxStatement, data);
		}
		if (keyword == data.keywordPointyLeftEqual) {
			return makePointyLeftEqual(stat, omaxStatement, data);
		}
		if (keyword == data.keywordPointyRightEqual) {
			return makePointyRightEqual(stat, omaxStatement, data);
		}
		if (keyword == data.keywordPointy1) {
			return makePointy1(stat, omaxStatement, data);
		}
		if (keyword == data.keywordPointy2) {
			return makePointy2(stat, omaxStatement, data);
		}
		if (keyword == data.keywordPointyEqual1) {
			return makePointyEqual1(stat, omaxStatement, data);
		}
		if (keyword == data.keywordPointyEqual2) {
			return makePointyEqual2(stat, omaxStatement, data);
		}
		var ms = "Keyword not supported: " + keyword + ".";
		throw new DefectGenerator(ms);
	}

	public String makeAll(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		ret += makeLine(2, "var ret = new " + makeOclassName(stat.name) + "();");
		ret += makeLine(2, "ret.begin = offset;");
		ret += makeLine(2, "int off = offset;");
		jp.setLocalName(stat.oDef.defList);
		for (var odef : stat.oDef.defList) {
			var name = odef.name;
			ret += makeLine(2, "off = " + makeIclassName(name) + ".read(dt, off);");
			ret += makeLine(2, "ret." + makeOvalueName(odef.localName) + " = dt." + makeOvalueName(name) + ";");
			ret += makeLine(2, "if (off < 0) {");
			ret += makeLine(2, "	dt." + makeOvalueName(stat.name) + " = ret;");
			ret += makeLine(2, "	ret.end = off;");
			ret += makeLine(2, "	return off;");
			ret += makeLine(2, "}");
		}
		ret += makeLine(2, "dt." + makeOvalueName(stat.name) + " = ret;");
		ret += makeLine(2, "ret.end = off;");
		ret += makeLine(2, "return off;");
		return ret;
	}

	public String makeCheck(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		ret += makeLine(2, "var ret = new " + makeOclassName(stat.name) + "();");
		ret += makeLine(2, "ret.begin = offset;");
		var name = stat.oDef.defList.get(0).name;
		ret += makeLine(2, "ret.end = " + makeIclassName(name) + ".read(dt, offset);");
		ret += makeLine(2, "if (ret.end >= 0) {");
		ret += makeLine(2, "	ret.end = offset;");
		ret += makeLine(2, "}");
		ret += makeLine(2, "dt." + makeOvalueName(stat.name) + " = ret;");
		ret += makeLine(2, "return ret.end;");
		return ret;
	}

	public String makeCopy(Ostatement stat, OmaxStatement omaxStatement, Data data) throws DefectPattern, DefectGenerator {
//		var name = stat.oDef.defList.get(0).name;
//		var stat2 = omaxStatement.statementMap.get(name);
//		var ret = makeLogic(stat2, omaxStatement, data);
//		return ret;
		var ret = "";
		ret += makeLine(2, "var ret = new " + makeOclassName(stat.name) + "();");
		ret += makeLine(2, "ret.begin = offset;");
		var name = stat.oDef.defList.get(0).name;
		ret += makeLine(2, "ret.end = " + makeIclassName(name) + ".read(dt, offset);");
		ret += makeLine(2, "ret." + makeOvalueName(name) + " = dt." + makeOvalueName(name) + ";");
		ret += makeLine(2, "dt." + makeOvalueName(stat.name) + " = ret;");
		ret += makeLine(2, "return ret.end;");
		return ret;
	}

	public String makeEnd(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		ret += makeLine(2, "if (offset >= dt.text.getLength()) {");
		ret += makeLine(2, "	return offset;");
		ret += makeLine(2, "}");
		ret += makeLine(2, "return -offset - 1;");
		return ret;
	}

	public String makeMax(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		ret += makeLine(2, "var ret = new " + makeOclassName(stat.name) + "();");
		ret += makeLine(2, "ret.begin = offset;");
		ret += makeLine(2, "int off = offset;");
		ret += makeLine(2, "final int min = " + stat.oDef.min + ";");
		ret += makeLine(2, "final int max = " + stat.oDef.max + ";");
		var name = stat.oDef.defList.get(0).name;
		ret += makeLine(2, "int count = 0;");
		ret += makeLine(2, "while (count < min) {");
		ret += makeLine(2, "	off = " + makeIclassName(name) + ".read(dt, off);");
		ret += makeLine(2, "	if (dt." + makeOvalueName(name) + " != null) {");
		ret += makeLine(2, "		ret." + makeOvalueName(name) + "List.add(dt." + makeOvalueName(name) + ");");
		ret += makeLine(2, "	}");
		ret += makeLine(2, "	if (off < 0) {");
		ret += makeLine(2, "		dt." + makeOvalueName(stat.name) + " = ret;");
		ret += makeLine(2, "		ret.end = off;");
		ret += makeLine(2, "		return off;");
		ret += makeLine(2, "	}");
		ret += makeLine(2, "	count++;");
		ret += makeLine(2, "}");
		ret += makeLine(2, "while (count < max) {");
		ret += makeLine(2, "	var off2 = " + makeIclassName(name) + ".read(dt, off);");
		ret += makeLine(2, "	if (off2 < 0) {");
		ret += makeLine(2, "		break;");
		ret += makeLine(2, "	}");
		ret += makeLine(2, "	if (dt." + makeOvalueName(name) + " != null) {");
		ret += makeLine(2, "		ret." + makeOvalueName(name) + "List.add(dt." + makeOvalueName(name) + ");");
		ret += makeLine(2, "	}");
		ret += makeLine(2, "	off = off2;");
		ret += makeLine(2, "	count++;");
		ret += makeLine(2, "}");
		ret += makeLine(2, "dt." + makeOvalueName(stat.name) + " = ret;");
		ret += makeLine(2, "ret.end = off;");
		ret += makeLine(2, "return off;");
		return ret;
	}

	public String makeMaxTo(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		ret += makeLine(2, "var ret = new " + makeOclassName(stat.name) + "();");
		ret += makeLine(2, "ret.begin = offset;");
		ret += makeLine(2, "dt." + makeOvalueName(stat.name) + " = ret;");
		ret += makeLine(2, "int off = offset;");
		ret += makeLine(2, "final int min = " + stat.oDef.min + ";");
		ret += makeLine(2, "final int max = " + stat.oDef.max + ";");
		var name = stat.oDef.defList.get(0).name;
		var name2 = stat.oDef.defList.get(1).name;
		ret += makeLine(2, "int count = 0;");
		ret += makeLine(2, "while (count < min) {");
		ret += makeLine(2, "	off = " + makeIclassName(name) + ".read(dt, off);");
		ret += makeLine(2, "	if (dt." + makeOvalueName(name) + " != null) {");
		ret += makeLine(2, "		ret." + makeOvalueName(name) + "List.add(dt." + makeOvalueName(name) + ");");
		ret += makeLine(2, "	}");
		ret += makeLine(2, "	if (off < 0) {");
		ret += makeLine(2, "		dt." + makeOvalueName(stat.name) + " = ret;");
		ret += makeLine(2, "		ret.end = off;");
		ret += makeLine(2, "		return off;");
		ret += makeLine(2, "	}");
		ret += makeLine(2, "	count++;");
		ret += makeLine(2, "}");
		ret += makeLine(2, "var listMark = ret." + makeOvalueName(name) + "List.size();");
		ret += makeLine(2, "while (count < max) {");
		ret += makeLine(2, "	var off2 = " + makeIclassName(name2) + ".read(dt, off); ");
		ret += makeLine(2, "	if (off2 >= 0) {");
		ret += makeLine(2, "		listMark = ret." + makeOvalueName(name) + "List.size();");
		ret += makeLine(2, "		ret.end = off2;");
		ret += makeLine(2, "		ret." + makeOvalueName(name2) + " = dt." + makeOvalueName(name2) + ";");
		ret += makeLine(2, "	}");
		ret += makeLine(2, "	var off1 = " + makeIclassName(name) + ".read(dt, off);");
		ret += makeLine(2, "	if (off1 >= 0) {");
		ret += makeLine(2, "		if (dt." + makeOvalueName(name) + " != null) {");
		ret += makeLine(2, "			ret." + makeOvalueName(name) + "List.add(dt." + makeOvalueName(name) + ");");
		ret += makeLine(2, "		}");
		ret += makeLine(2, "		off = off1;");
		ret += makeLine(2, "		count++;");
		ret += makeLine(2, "	} else {");
		ret += makeLine(2, "		break;");
		ret += makeLine(2, "	}");
		ret += makeLine(2, "}");
		ret += makeLine(2, "var off2 = " + makeIclassName(name2) + ".read(dt, off); ");
		ret += makeLine(2, "if (off2 >= 0) {");
		ret += makeLine(2, "	listMark = ret." + makeOvalueName(name) + "List.size();");
		ret += makeLine(2, "	ret.end = off2;");
		ret += makeLine(2, "	ret." + makeOvalueName(name2) + " = dt." + makeOvalueName(name2) + ";");
		ret += makeLine(2, "}");
		ret += makeLine(2, "if (ret.end >= 0) {");
		ret += makeLine(2, "	ret." + makeOvalueName(name) + "List = new ArrayList<>(ret." + makeOvalueName(name) + "List.subList(0, listMark));");
		ret += makeLine(2, "} else {");
		ret += makeLine(2, "	ret.end = -off - 1;");
		ret += makeLine(2, "}");
		ret += makeLine(2, "dt." + makeOvalueName(stat.name) + " = ret;");
		ret += makeLine(2, "return ret.end;");
		return ret;
	}

	public String makeMaybe(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		ret += makeLine(2, "var ret = new " + makeOclassName(stat.name) + "();");
		ret += makeLine(2, "ret.begin = offset;");
		ret += makeLine(2, "dt." + makeOvalueName(stat.name) + " = ret;");
		var name = stat.oDef.defList.get(0).name;
		ret += makeLine(2, "var off = " + makeIclassName(name) + ".read(dt, offset);");
		ret += makeLine(2, "if (off > 0) {");
		ret += makeLine(2, "	ret.end = off;");
		ret += makeLine(2, "} else {");
		ret += makeLine(2, "	ret.end = offset;");
		ret += makeLine(2, "}");
		ret += makeLine(2, "dt." + makeOvalueName(stat.name) + " = ret;");
		ret += makeLine(2, "return ret.end;");
		return ret;
	}

	public String makeMin(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		ret += makeLine(2, "var ret = new " + makeOclassName(stat.name) + "();");
		ret += makeLine(2, "ret.begin = offset;");
		ret += makeLine(2, "dt." + makeOvalueName(stat.name) + " = ret;");
		ret += makeLine(2, "int off = offset;");
		ret += makeLine(2, "final int min = " + stat.oDef.min + ";");
		var name = stat.oDef.defList.get(0).name;
		ret += makeLine(2, "int count = 0;");
		ret += makeLine(2, "while (count < min) {");
		ret += makeLine(2, "	off = " + makeIclassName(name) + ".read(dt, off);");
		ret += makeLine(2, "	if (dt." + makeOvalueName(name) + " != null) {");
		ret += makeLine(2, "		ret." + makeOvalueName(name) + "List.add(dt." + makeOvalueName(name) + ");");
		ret += makeLine(2, "	}");
		ret += makeLine(2, "	if (off < 0) {");
		ret += makeLine(2, "		dt." + makeOvalueName(stat.name) + " = ret;");
		ret += makeLine(2, "		ret.end = off;");
		ret += makeLine(2, "		return off;");
		ret += makeLine(2, "	}");
		ret += makeLine(2, "	count++;");
		ret += makeLine(2, "}");
		ret += makeLine(2, "dt." + makeOvalueName(stat.name) + " = ret;");
		ret += makeLine(2, "ret.end = off;");
		ret += makeLine(2, "return off;");
		return ret;
	}

	public String makeMinTo(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		ret += makeLine(2, "var ret = new " + makeOclassName(stat.name) + "();");
		ret += makeLine(2, "ret.begin = offset;");
		ret += makeLine(2, "dt." + makeOvalueName(stat.name) + " = ret;");
		ret += makeLine(2, "int off = offset;");
		ret += makeLine(2, "final int min = " + stat.oDef.min + ";");
		ret += makeLine(2, "final int max = " + stat.oDef.max + ";");
		var name = stat.oDef.defList.get(0).name;
		var name2 = stat.oDef.defList.get(1).name;
		ret += makeLine(2, "int count = 0;");
		ret += makeLine(2, "while (count < min) {");
		ret += makeLine(2, "	off = " + makeIclassName(name) + ".read(dt, off);");
		ret += makeLine(2, "	if (dt." + makeOvalueName(name) + " != null) {");
		ret += makeLine(2, "		ret." + makeOvalueName(name) + "List.add(dt." + makeOvalueName(name) + ");");
		ret += makeLine(2, "	}");
		ret += makeLine(2, "	if (off < 0) {");
		ret += makeLine(2, "		dt." + makeOvalueName(stat.name) + " = ret;");
		ret += makeLine(2, "		ret.end = off;");
		ret += makeLine(2, "		return off;");
		ret += makeLine(2, "	}");
		ret += makeLine(2, "	count++;");
		ret += makeLine(2, "}");
		ret += makeLine(2, "while (count < max) {");
		ret += makeLine(2, "	var off2 = " + makeIclassName(name2) + ".read(dt, off); ");
		ret += makeLine(2, "	if (off2 >= 0) {");
		ret += makeLine(2, "		ret." + makeOvalueName(name2) + " = dt." + makeOvalueName(name2) + ";");
		ret += makeLine(2, "		dt." + makeOvalueName(stat.name) + " = ret;");
		ret += makeLine(2, "		ret.end = off2;");
		ret += makeLine(2, "		return off2;");
		ret += makeLine(2, "	}");
		ret += makeLine(2, "	var off1 = " + makeIclassName(name) + ".read(dt, off);");
		ret += makeLine(2, "	if (off1 >= 0) {");
		ret += makeLine(2, "		if (dt." + makeOvalueName(name) + " != null) {");
		ret += makeLine(2, "			ret." + makeOvalueName(name) + "List.add(dt." + makeOvalueName(name) + ");");
		ret += makeLine(2, "		}");
		ret += makeLine(2, "		off = off1;");
		ret += makeLine(2, "		count++;");
		ret += makeLine(2, "	} else {");
		ret += makeLine(2, "		break;");
		ret += makeLine(2, "	}");
		ret += makeLine(2, "}");
		ret += makeLine(2, "var off2 = " + makeIclassName(name2) + ".read(dt, off); ");
		ret += makeLine(2, "if (off2 >= 0) {");
		ret += makeLine(2, "	ret." + makeOvalueName(name2) + " = dt." + makeOvalueName(name2) + ";");
		ret += makeLine(2, "	dt." + makeOvalueName(stat.name) + " = ret;");
		ret += makeLine(2, "	ret.end = off2;");
		ret += makeLine(2, "	return off2;");
		ret += makeLine(2, "}");
		ret += makeLine(2, "dt." + makeOvalueName(stat.name) + " = ret;");
		ret += makeLine(2, "ret.end = -off - 1;");
		ret += makeLine(2, "return ret.end;");
		return ret;
	}

	public String makeNot(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		ret += makeLine(2, "var ret = new " + makeOclassName(stat.name) + "();");
		ret += makeLine(2, "ret.begin = offset;");
		var name = stat.oDef.defList.get(0).name;
		ret += makeLine(2, "var off = " + makeIclassName(name) + ".read(dt, offset);");
		ret += makeLine(2, "if (off < 0) {");
		ret += makeLine(2, "	ret.end = offset;");
		ret += makeLine(2, "}");
		ret += makeLine(2, "dt." + makeOvalueName(stat.name) + " = ret;");
		ret += makeLine(2, "return ret.end;");
		return ret;
	}

	public String makeOne(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		ret += makeLine(2, "var ret = new " + makeOclassName(stat.name) + "();");
		ret += makeLine(2, "ret.begin = offset;");
		ret += makeLine(2, "var off = offset;");
		//Duplicates should be indicated as errors in the generated code because they are useless for the "one" pattern.
		for (var odef : stat.oDef.defList) {
			var name = odef.name;
			ret += makeLine(2, "off = " + makeIclassName(name) + ".read(dt, offset);");
			ret += makeLine(2, "ret." + makeOvalueName(name) + " = dt." + makeOvalueName(name) + ";");
			ret += makeLine(2, "if (off >= 0) {");
			ret += makeLine(2, "	dt." + makeOvalueName(stat.name) + " = ret;");
			ret += makeLine(2, "	ret.end = off;");
			ret += makeLine(2, "	return off;");
			ret += makeLine(2, "}");
		}
		ret += makeLine(2, "dt." + makeOvalueName(stat.name) + " = ret;");
		ret += makeLine(2, "ret.end = off;");
		ret += makeLine(2, "return off;");
		return ret;
	}

	public String makeText(Ostatement stat, OmaxStatement omaxStatement, Data data) {
		var ret = "";
		ret += makeLine(2, "var ret = new " + makeOclassName(stat.name) + "();");
		ret += makeLine(2, "ret.begin = offset;");
		ret += makeLine(2, "dt." + makeOvalueName(stat.name) + " = ret;");
		ret += makeLine(2, "var afterMatch = Offset.afterMatch(dt.text, offset, text, 0, text.getLength());");
		ret += makeLine(2, "ret.end = afterMatch;");
		ret += makeLine(2, "return afterMatch;");
		return ret;
	}


	public int getLetterNumber(Ostatement stat, OmaxStatement omaxStatement) throws DefectPattern {
		var odef = stat.oDef;
		if (odef.letter.number != null) {
			return odef.letter.number;
		}
		var name = stat.name;
		var usedName = odef.letter.name;
		var usedStat = omaxStatement.statementMap.get(usedName);
		if (usedStat == null) {
			var ms = "Letter pattern " + usedName + " is not defined. It is required by pattern " + name + ".";
			throw new DefectPattern(ms);
		}
		if (usedStat.oDef.letter.number != null) {
			var ms = "Pattern " + usedName + " is not a lettern pattern. It is required by pattern " + name + ".";
			throw new DefectPattern(ms);
		}
		return usedStat.oDef.letter.number;
	}


	public int getLetterNumber2(Ostatement stat, OmaxStatement omaxStatement) throws DefectPattern {
		var odef = stat.oDef;
		if (odef.letter2.number != null) {
			return odef.letter2.number;
		}
		var name = stat.name;
		var usedName = odef.letter2.name;
		var usedStat = omaxStatement.statementMap.get(usedName);
		if (usedStat == null) {
			var ms = "Letter pattern " + usedName + " is not defined. It is required by pattern " + name + ".";
			throw new DefectPattern(ms);
		}
		if (usedStat.oDef.letter.number != null) {
			var ms = "Pattern " + usedName + " is not a lettern pattern. It is required by pattern " + name + ".";
			throw new DefectPattern(ms);
		}
		return usedStat.oDef.letter.number;
	}

	public String makeEqual(Ostatement stat, OmaxStatement omaxStatement, Data data) throws DefectPattern {
		var ret = "";
		ret += makeLine(2, "var le = dt.read(offset);");
		var letterNumber = getLetterNumber(stat, omaxStatement);
		ret += makeLine(2, "// '" + Ascii.stringWithEscapeOf(letterNumber) + "' = " + letterNumber);
		ret += makeLine(2, "if (le.letter == " + letterNumber + ") {");
		ret += makeLine(2, "	return le.end;");
		ret += makeLine(2, "}");
		ret += makeLine(2, "return -offset - 1;");
		return ret;
	}

	public String makeUnequal(Ostatement stat, OmaxStatement omaxStatement, Data data) throws DefectPattern {
		var ret = "";
		ret += makeLine(2, "var le = dt.read(offset);");
		var letterNumber = getLetterNumber(stat, omaxStatement);
		ret += makeLine(2, "// '" + Ascii.stringWithEscapeOf(letterNumber) + "' = " + letterNumber);
		ret += makeLine(2, "if (le.letter != " + letterNumber + ") {");
		ret += makeLine(2, "	return le.end;");
		ret += makeLine(2, "}");
		ret += makeLine(2, "return -offset - 1;");
		return ret;
	}

	public String makePointyLeft(Ostatement stat, OmaxStatement omaxStatement, Data data) throws DefectPattern {
		var ret = "";
		ret += makeLine(2, "var le = dt.read(offset);");
		var letterNumber = getLetterNumber(stat, omaxStatement);
		ret += makeLine(2, "// '" + Ascii.stringWithEscapeOf(letterNumber) + "' = " + letterNumber);
		ret += makeLine(2, "if (le.letter < " + letterNumber + ") {");
		ret += makeLine(2, "	return le.end;");
		ret += makeLine(2, "}");
		ret += makeLine(2, "return -offset - 1;");
		return ret;
	}

	public String makePointyRight(Ostatement stat, OmaxStatement omaxStatement, Data data) throws DefectPattern {
		var ret = "";
		ret += makeLine(2, "var le = dt.read(offset);");
		var letterNumber = getLetterNumber(stat, omaxStatement);
		ret += makeLine(2, "// '" + Ascii.stringWithEscapeOf(letterNumber) + "' = " + letterNumber);
		ret += makeLine(2, "if (le.letter > " + letterNumber + ") {");
		ret += makeLine(2, "	return le.end;");
		ret += makeLine(2, "}");
		ret += makeLine(2, "return -offset - 1;");
		return ret;
	}

	public String makePointyLeftEqual(Ostatement stat, OmaxStatement omaxStatement, Data data) throws DefectPattern {
		var ret = "";
		ret += makeLine(2, "var le = dt.read(offset);");
		var letterNumber = getLetterNumber(stat, omaxStatement);
		ret += makeLine(2, "// '" + Ascii.stringWithEscapeOf(letterNumber) + "' = " + letterNumber);
		ret += makeLine(2, "if (le.letter <= " + letterNumber + ") {");
		ret += makeLine(2, "	return le.end;");
		ret += makeLine(2, "}");
		ret += makeLine(2, "return -offset - 1;");
		return ret;
	}

	public String makePointyRightEqual(Ostatement stat, OmaxStatement omaxStatement, Data data) throws DefectPattern {
		var ret = "";
		ret += makeLine(2, "var le = dt.read(offset);");
		var letterNumber = getLetterNumber(stat, omaxStatement);
		ret += makeLine(2, "// '" + Ascii.stringWithEscapeOf(letterNumber) + "' = " + letterNumber);
		ret += makeLine(2, "if (le.letter >= " + letterNumber + ") {");
		ret += makeLine(2, "	return le.end;");
		ret += makeLine(2, "}");
		ret += makeLine(2, "return -offset - 1;");
		return ret;
	}

	public String makePointy1(Ostatement stat, OmaxStatement omaxStatement, Data data) throws DefectPattern {
		var ret = "";
		ret += makeLine(2, "var le = dt.read(offset);");
		var letterNumber = getLetterNumber(stat, omaxStatement);
		var letterNumber2 = getLetterNumber2(stat, omaxStatement);
		ret += makeLine(2, "// '" + Ascii.stringWithEscapeOf(letterNumber) + "' = " + letterNumber);
		ret += makeLine(2, "// '" + Ascii.stringWithEscapeOf(letterNumber2) + "' = " + letterNumber2);
		ret += makeLine(2, "if (le.letter > " + letterNumber + " && le.letter < " + letterNumber2 + ") {");
		ret += makeLine(2, "	return le.end;");
		ret += makeLine(2, "}");
		ret += makeLine(2, "return -offset - 1;");
		return ret;
	}

	public String makePointy2(Ostatement stat, OmaxStatement omaxStatement, Data data) throws DefectPattern {
		var ret = "";
		ret += makeLine(2, "var le = dt.read(offset);");
		var letterNumber = getLetterNumber(stat, omaxStatement);
		var letterNumber2 = getLetterNumber2(stat, omaxStatement);
		ret += makeLine(2, "// '" + Ascii.stringWithEscapeOf(letterNumber) + "' = " + letterNumber);
		ret += makeLine(2, "// '" + Ascii.stringWithEscapeOf(letterNumber2) + "' = " + letterNumber2);
		ret += makeLine(2, "if (le.letter > " + letterNumber + " && le.letter <= " + letterNumber2 + ") {");
		ret += makeLine(2, "	return le.end;");
		ret += makeLine(2, "}");
		ret += makeLine(2, "return -offset - 1;");
		return ret;
	}

	public String makePointyEqual1(Ostatement stat, OmaxStatement omaxStatement, Data data) throws DefectPattern {
		var ret = "";
		ret += makeLine(2, "var le = dt.read(offset);");
		var letterNumber = getLetterNumber(stat, omaxStatement);
		var letterNumber2 = getLetterNumber2(stat, omaxStatement);
		ret += makeLine(2, "// '" + Ascii.stringWithEscapeOf(letterNumber) + "' = " + letterNumber);
		ret += makeLine(2, "// '" + Ascii.stringWithEscapeOf(letterNumber2) + "' = " + letterNumber2);
		ret += makeLine(2, "if (le.letter >= " + letterNumber + " && le.letter < " + letterNumber2 + ") {");
		ret += makeLine(2, "	return le.end;");
		ret += makeLine(2, "}");
		ret += makeLine(2, "return -offset - 1;");
		return ret;
	}

	public String makePointyEqual2(Ostatement stat, OmaxStatement omaxStatement, Data data) throws DefectPattern {
		var ret = "";
		ret += makeLine(2, "var le = dt.read(offset);");
		var letterNumber = getLetterNumber(stat, omaxStatement);
		var letterNumber2 = getLetterNumber2(stat, omaxStatement);
		ret += makeLine(2, "// '" + Ascii.stringWithEscapeOf(letterNumber) + "' = " + letterNumber);
		ret += makeLine(2, "// '" + Ascii.stringWithEscapeOf(letterNumber2) + "' = " + letterNumber2);
		ret += makeLine(2, "if (le.letter >= " + letterNumber + " && le.letter <= " + letterNumber2 + ") {");
		ret += makeLine(2, "	return le.end;");
		ret += makeLine(2, "}");
		ret += makeLine(2, "return -offset - 1;");
		return ret;
	}
}
