package com.gitlab.gerhard_piette.pattern_16;

import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;

public class TdefPointyLeftEqual {

	public static Odef transport(String name, Data dt, int offset) throws DefectLetter, DefectOffset, DefectPattern, DefectLimit {
		var ret = new Odef();
		ret.keyword = dt.keywordPointyLeftEqual;
		ret.begin = offset;
		var off = offset;
		//letter 1
		var afterKey = dt.afterMatch(off, dt.keywordPointyLeftEqual);
		if (afterKey < 0) {
			afterKey = dt.afterMatch(off, dt.keywordEqualPointyLeft);
			if (afterKey < 0) {
				ret.match = false;
				return ret;
			}
		}
		off = afterKey;
		off = dt.afterSpace(off);
		var oletter = Tletter.transport(name, dt, off);
		if (oletter.match) {
			ret.letter = oletter;
		} else {
			var ms = "Defect pattern: " + name + ". Lack of letter declaration after offset " + off + ".";
			throw new DefectPattern(ms);
		}
		off = oletter.end;
		ret.match = true;
		ret.end = off;
		return ret;
	}

}