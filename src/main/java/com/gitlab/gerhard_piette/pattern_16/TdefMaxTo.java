package com.gitlab.gerhard_piette.pattern_16;

import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.integer_1.UnicodeInteger;

import java.util.ArrayList;

public class TdefMaxTo {

	public static UnicodeInteger unicodeInteger = new UnicodeInteger();

	public static Odef transport(String name, Data dt, int offset) throws DefectLetter, DefectOffset, DefectPattern, DefectLimit {
		var ret = new Odef();
		ret.keyword = dt.keywordMaxto;
		ret.begin = offset;
		var off = offset;
		off = dt.afterLeft(off);
		if (off < 0) {
			ret.match = false;
			return ret;
		}
		off = dt.afterSpace(off);
		off = dt.afterMatch(off, dt.keywordMaxto);
		if (off < 0) {
			ret.match = false;
			return ret;
		}
		off = dt.afterSpace(off);
		var afterInteger = unicodeInteger.afterIntegerDecWithUnderscore(dt.text, off);
		if (afterInteger > 0) {
			var str = dt.text.getFromTo(off, afterInteger).toString();
			str = str.replaceAll("_", "");
			ret.min = Integer.valueOf(str);
			off = dt.afterSpace(afterInteger);
			afterInteger = unicodeInteger.afterIntegerDecWithUnderscore(dt.text, off);
			if (afterInteger > 0) {
				str = dt.text.getFromTo(off, afterInteger).toString();
				str = str.replaceAll("_", "");
				ret.max = Integer.valueOf(str);
				off = dt.afterSpace(afterInteger);
			}
		}
		off = dt.afterSpace(off);
		var odefList = new ArrayList<Odef>();
		var odef1 = TdefLocal.transport(name, dt, off);
		if (!odef1.match) {
			var ms = "Defect pattern: " + name + ". Expected pattern after offset " + off + ".";
			throw new DefectPattern(ms);
		}
		odefList.add(odef1);
		off = odef1.end;
		off = dt.afterSpace(off);
		var odef2 = TdefLocal.transport(name, dt, off);
		if (!odef2.match) {
			var ms = "Defect pattern: " + name + ". Expected pattern after offset " + off + ".";
			throw new DefectPattern(ms);
		}
		odefList.add(odef2);
		off = odef2.end;
		off = dt.afterSpace(off);
		off = dt.afterRight(off);
		if (off < 0) {
			var ms = "Defect pattern: " + name + ". Expected closing right parenthesis after offset " + off + ".";
			throw new DefectPattern(ms);
		}
		ret.match = true;
		ret.end = off;
		ret.defList = odefList;
		return ret;
	}

}