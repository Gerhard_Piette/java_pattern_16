package com.gitlab.gerhard_piette.pattern_16;

import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;

/**
 * To match the definition of a global pattern.
 */
public class TdefGlobal {

	public static Odef transport(String name, Data dt, int offset) throws DefectLetter, DefectOffset, DefectPattern, DefectLimit {
		Odef ret = null;
		ret = TdefCopy.transport(name, dt, offset);
		if (ret.match) {
			return ret;
		}
		ret = TdefCommon.transport(name, dt, offset);
		return ret;
	}

}