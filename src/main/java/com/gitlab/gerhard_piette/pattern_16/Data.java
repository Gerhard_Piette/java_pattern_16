package com.gitlab.gerhard_piette.pattern_16;

import com.gitlab.gerhard_piette.ascii_1.Ascii;
import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.letter_1.LetterEnd;
import com.gitlab.gerhard_piette.text_6.StringText;
import com.gitlab.gerhard_piette.text_6.Text;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Data {

	public Text text = null;

	/**
	 * key = name of the pattern.
	 * target = Set of names of used patterns.
	 */
	public HashMap<String, Set<String>> usedNameMap = new HashMap<>();

	public void addUsedPattern(String name, String usedName) {
		var set = usedNameMap.get(name);
		if (set == null) {
			set = new HashSet<>();
			usedNameMap.put(name, set);
		}
		set.add(usedName);
	}

	public int Apostrophe = Ascii.Apostrophe;
	public int Capital_A = Ascii.Capital_A;
	public int Capital_F = Ascii.Capital_F;
	public int Capital_Z = Ascii.Capital_Z;
	public int Carriage_return = Ascii.Carriage_return;
	public int Comma = Ascii.Comma;
	public int Digit_0 = Ascii.Digit_0;
	public int Digit_1 = Ascii.Digit_1;
	public int Digit_7 = Ascii.Digit_7;
	public int Digit_9 = Ascii.Digit_9;
	public int Flat_left = Ascii.Left_Square_Bracket;
	public int Flat_right = Ascii.Right_Square_Bracket;
	public int Fullstop = Ascii.Full_stop;
	public int LinEnd = Ascii.Line_feed;
	public int Round_left = Ascii.Left_Parenthesis;
	public int Round_right = Ascii.Right_Parenthesis;
	public int Small_A = Ascii.Small_A;
	public int Small_F = Ascii.Small_F;
	public int Small_Z = Ascii.Small_Z;
	public int Space = Ascii.Space;
	public int Tab = Ascii.Horizontal_Tab;

	public StringText keywordComment = new StringText("//");

	public StringText keywordAll = new StringText("all");
	public StringText keywordCheck = new StringText("check");
	public StringText keywordCopy = new StringText("copy");
	public StringText keywordEnd = new StringText("end");
	public StringText keywordMax = new StringText("max");
	public StringText keywordMaxto = new StringText("maxto");
	public StringText keywordMaybe = new StringText("maybe");
	public StringText keywordMin = new StringText("min");
	public StringText keywordMinto = new StringText("minto");
	public StringText keywordName = new StringText("keywordName");
	public StringText keywordNot = new StringText("not");
	public StringText keywordOne = new StringText("one");
	public StringText keywordText = new StringText("keywordText");

	public StringText keywordEqual = new StringText("=");
	public StringText keywordUnequal = new StringText("keywordUnequal");
	public StringText keywordDivEqual = new StringText("/=");
	public StringText keywordExclamationEqual = new StringText("!=");
	public StringText keywordPointyLeft = new StringText("<");
	public StringText keywordPointyRight = new StringText(">");
	public StringText keywordEqualPointyLeft = new StringText("=<");
	public StringText keywordPointyLeftEqual = new StringText("<=");
	public StringText keywordPointyRightEqual = new StringText(">=");
	public StringText keywordPointy1 = new StringText("><");
	public StringText keywordPointy2 = new StringText("><=");
	public StringText keywordPointyEqual1 = new StringText(">=<");
	public StringText keywordPointyEqual2 = new StringText(">=<=");

	public int getLength() {
		return text.getLength();
	}

	public Text getText(int begin, int end) throws DefectOffset {
		return text.getFromTo(begin, end);
	}

	public void printOffsetAndLetter() throws DefectOffset {
		for (int off = 0; off < text.getLength(); off++) {
			System.out.println("" + off + " " + text.getFromTo(off, off + 1).toString());
		}
	}

	public LetterEnd read(int offset) throws DefectLetter, DefectOffset {
		return text.read(offset);
	}

	public boolean isSpace(int cp) {
		if (cp == Space
				|| cp == Tab
				|| cp == LinEnd
				|| cp == Carriage_return
		) {
			return true;
		}
		return false;
	}

	public int afterSpace(int offset) throws DefectLetter, DefectOffset {
		var off = offset;
		while (off < text.getLength()) {
			var le = text.read(off);
			if (!isSpace(le.letter)) {
				return off;
			}
			off = le.end;
		}
		return off;
	}


	public boolean isLeftBracket(int cp) {
		if (cp == Round_left || cp == Flat_left) {
			return true;
		}
		return false;
	}


	public boolean isRightBracket(int cp) {
		if (cp == Round_right || cp == Flat_right) {
			return true;
		}
		return false;
	}

	public boolean isNumberLink(int cp) {
		if (cp == Comma || cp == Fullstop) {
			return true;
		}
		return false;
	}

	public boolean isPathLink(int cp) {
		if (cp == Comma || cp == Fullstop) {
			return true;
		}
		return false;
	}

	public boolean isIntegerBin(int cp) {
		if (cp < Digit_0 || cp > Digit_1) {
			return false;
		}
		return true;
	}

	public boolean isIntegerDec(int cp) {
		if (cp < Digit_0 || cp > Digit_9) {
			return false;
		}
		return true;
	}

	public boolean isIntegerOct(int cp) {
		if (cp < Digit_0 || cp > Digit_7) {
			return false;
		}
		return true;
	}

	public boolean isIntegerHex(int cp) {
		if (isIntegerDec(cp)) {
			return true;
		}
		if (cp >= Capital_A && cp <= Capital_F) {
			return true;
		}
		if (cp >= Small_A && cp <= Small_F) {
			return true;
		}
		return false;
	}

	public boolean isLatinCapital(int cp) {
		if (cp < Capital_A || cp > Capital_Z) {
			return false;
		}
		return true;
	}

	public boolean isLatinSmall(int cp) {
		if (cp < Small_A || cp > Small_Z) {
			return false;
		}
		return true;
	}

	public boolean isIdentifierFirstLetter(int cp) {
		if (isLatinSmall(cp)
				|| isLatinCapital(cp)
				|| cp == Ascii.Underscore
				|| cp > 127
		) {
			return true;
		}
		return false;
	}

	public boolean isIdentifierOtherLetter(int cp) {
		if (isIdentifierFirstLetter(cp)
				|| isIntegerDec(cp)
		) {
			return true;
		}
		return false;
	}


	public boolean isLink(int cp) {
		if (cp == Comma
				|| cp == Fullstop
		) {
			return true;
		}
		return false;
	}


	public boolean isExpLimit(int cp) {
		if (cp == Round_left
				|| cp == Round_right
				|| cp == Flat_left
				|| cp == Flat_right
				|| isSpace(cp)
				|| isLink(cp)
				|| cp < 0
		) {
			return true;
		}
		return false;
	}


	public int afterLetter(int offset, int letter) throws DefectLetter, DefectOffset {
		if (offset >= text.getLength()) {
			return -offset - 1;
		}
		var le = text.read(offset);
		if (le.letter != letter) {
			return -offset - 1;
		}
		return le.end;
	}


	public int afterLeft(int offset) throws DefectLetter, DefectOffset {
		var off = afterLetter(offset, Round_left);
		if (off < 0) {
			return off;
		}
		return off;
	}


	public int afterRight(int offset) throws DefectLetter, DefectOffset {
		var off = afterLetter(offset, Round_right);
		if (off < 0) {
			return off;
		}
		return off;
	}


	/**
	 * Checks a limit and throws a defectLimit exception if there is no valid limits.
	 *
	 * @param offset
	 * @return The offset after the limit.
	 * @throws DefectLetter
	 * @throws DefectOffset
	 * @throws DefectLimit
	 */
	public void checkLimit(int offset) throws DefectLetter, DefectOffset, DefectLimit {
		if (offset >= text.getLength()) {
			return;
		}
		var le = text.read(offset);
		if (!isExpLimit(le.letter)) {
			var defectLimit = new DefectLimit("No valid expression limit like a gap or ( or ) after offset: " + offset);
			defectLimit.offset = offset;
			throw defectLimit;
		}
	}

//	public int afterExpLimit(int offset) throws DefectLetter, DefectOffset {
//		if (offset == text.getLength()) {
//			return offset;
//		}
//		var le = text.read(offset);
//		if (!isExpLimit(le.letter)) {
//			return -1;
//		}
//		return afterSpace(offset);
//	}

	public int afterMatch(int offset, Text text2) throws DefectLetter, DefectOffset, DefectLimit {
		var off = offset;
		off = text.afterMatch(off, text2, 0, text2.getLength());
		if (off < 0) {
			return off;
		}
		checkLimit(off);
		return off;
	}
}
