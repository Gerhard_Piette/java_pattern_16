package com.gitlab.gerhard_piette.pattern_16;

public class DefectGenerator extends Exception {

	public DefectGenerator(String message) {
		super(message);
	}

}
