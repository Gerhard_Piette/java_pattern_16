package com.gitlab.gerhard_piette.pattern_16;

public class DefectPattern extends Exception {

	public DefectPattern(String message) {
		super(message);
	}

}
