package com.gitlab.gerhard_piette.pattern_16;

public class DefectLimit extends Exception {

	public int offset = 0;

	public DefectLimit(String message) {
		super(message);
	}
}
