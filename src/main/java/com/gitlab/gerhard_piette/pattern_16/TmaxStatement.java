package com.gitlab.gerhard_piette.pattern_16;

import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;

import java.util.ArrayList;
import java.util.HashMap;

public class TmaxStatement {

	public static OmaxStatement transport(Data dt, int offset) throws DefectLetter, DefectOffset, DefectPattern, DefectLimit {
		var ret = new OmaxStatement();
		var off = offset;
		off = dt.afterSpace(off);
		int count = 1;
		while (off < dt.getLength()) {
			var ostatement = Tstatement.transport(dt, off);
			if (!ostatement.match) {
				break;
			}
			if (ostatement.oDef.keyword != dt.keywordComment) {
				if (ret.statementMap.containsKey(ostatement.name)) {
					var firstStatementWithName = ret.statementMap.get(ostatement.name);
					var ms = "Name is already defined: " + ostatement.name + ".\n";
					ms += "Name is first defined after offset: " + firstStatementWithName.begin + ".\n";
					ms += "Statement: " + firstStatementWithName.toString(dt.text);
					ms += "\n";
					ms += "Last statement after offset: " + ostatement.begin + ".\n";
					ms += "Last statement: " + ostatement.toString(dt.text);
					throw new DefectPattern(ms);
				}
				ret.statementList.add(ostatement);
				ret.statementMap.put(ostatement.name, ostatement);
				System.out.println("Valid statement " + count + " : " + ostatement.toString(dt.text));
				count++;
			}
			off = ostatement.end;
			off = dt.afterSpace(off);
		}
		off = dt.afterSpace(off);
		if (off < dt.text.getLength()) {
			var ms = "File not completely transported.";
			ms += " End after offset: " + off + ".";
			ms += " Text length: " + dt.text.getLength() + ".";
			if (ret.statementList.size() > 0) {
				var lastStat = ret.statementList.get(ret.statementList.size() - 1);
				ms += "\n";
				ms += "Last valid statement: " + dt.text.getFromTo(lastStat.begin, lastStat.end).toString();
			} else {
				ms += "\n";
				ms += "Defect in the first statement after offset: " + off;
			}
			throw new DefectPattern(ms);
		}
		resolveLetterName(ret, dt);
		addStatements(ret, dt);
		return ret;
	}


	public static void resolveLetterName(OmaxStatement omaxStatement, Data dt) throws DefectPattern, DefectOffset {
		for (var ostatement : omaxStatement.statementList) {
			var odef = ostatement.oDef;
			if (odef.letter != null) {
				if (odef.letter.name != null) {
					var letterStat = omaxStatement.statementMap.get(odef.letter.name);
					if (letterStat == null) {
						var ms = "Defect pattern: " + ostatement.name + ".";
						ms += " The pattern " + odef.letter.name + " is used but not defined.";
						throw new DefectPattern(ms);
					}
					if (letterStat.oDef.letter == null) {
						var ms = "Defect pattern: " + ostatement.name + ".";
						ms += " The first letter of pattern " + odef.letter.name + " is used but not defined before pattern " + ostatement.name + ".";
						throw new DefectPattern(ms);
					}
					odef.letter = letterStat.oDef.letter;
				}
			}
			if (odef.letter2 != null) {
				if (odef.letter2.name != null) {
					var letterStat = omaxStatement.statementMap.get(odef.letter2.name);
					if (letterStat.oDef.letter == null) {
						var ms = "Defect pattern: " + ostatement.name + ".\n";
						ms += "The first letter of pattern " + letterStat.name + " is used but not yet defined.";
						throw new DefectPattern(ms);
					}
					odef.letter2 = letterStat.oDef.letter;
				}
			}
		}
	}

	/**
	 * Addition a global statement for every local pattern definition except for defName.
	 *
	 * @param omaxStatement
	 * @param dt
	 */
	public static void addStatements(OmaxStatement omaxStatement, Data dt) {
		var newList = new ArrayList<Ostatement>();
		var newMap = new HashMap<String, Ostatement>();
		for (var stat : omaxStatement.statementList) {
			if (stat.oDef.defList != null) {
				for (var localDef : stat.oDef.defList) {
					if (localDef.keyword != dt.keywordName) {
						localDef.name = makeUniqueName(stat, omaxStatement, dt, newMap);
						var newStat = new Ostatement();
						newStat.name = localDef.name;
						newStat.oDef = localDef;
						newStat.begin = localDef.begin;
						newStat.end = localDef.end;
						newList.add(newStat);
						newMap.put(newStat.name, newStat);
					}
				}
			}
		}
		omaxStatement.statementList.addAll(newList);
		omaxStatement.statementMap.putAll(newMap);
	}

	public static String makeUniqueName(Ostatement stat, OmaxStatement omaxStatement, Data dt, HashMap<String, Ostatement> newMap) {
		var count = 1;
		while (true) {
			var ret = stat.name + "_" + count + "_";
			if (!omaxStatement.statementMap.containsKey(ret)
					&& !newMap.containsKey(ret)) {
				return ret;
			}
			count++;
		}
	}
}