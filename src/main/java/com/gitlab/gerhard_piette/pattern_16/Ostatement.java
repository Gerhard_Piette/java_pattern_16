package com.gitlab.gerhard_piette.pattern_16;

import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.text_6.Text;

public class Ostatement {

	public boolean match = false;

	public int begin = -1;

	public int end = -1;

	public String name;

	public Odef oDef;

	/**
	 * The source code of the I class.
	 */
	public String iClass;

	/**
	 * The source code of the O class.
	 */
	public String oClass;

	public String toString() {
		var ret = "name:" + name + " " + oDef;
		return ret;
	}

	public String toString(Text text) throws DefectOffset {
		var ret = "begin: " + begin + " end: " + end + " definition: " + text.getFromTo(begin, end).toString();
		return ret;
	}

}
