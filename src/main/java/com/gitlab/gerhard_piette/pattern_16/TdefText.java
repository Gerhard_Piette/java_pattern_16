package com.gitlab.gerhard_piette.pattern_16;

import com.gitlab.gerhard_piette.ascii_1.Ascii;
import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;

public class TdefText {

	public static Odef transport(String name, Data dt, int offset) throws DefectLetter, DefectOffset, DefectPattern, DefectLimit {
		var ret = new Odef();
		ret.keyword = dt.keywordText;
		ret.begin = offset;
		var off = offset;
		var beginLe = dt.text.read(off);
		if (beginLe.letter != Ascii.Quotation_Mark) {
			ret.match = false;
			return ret;
		}
		var sb = new StringBuilder();
		off = beginLe.end;
		while (off < dt.text.getLength()) {
			var le = dt.text.read(off);
			if (le.letter == Ascii.Quotation_Mark) {
				ret.str = sb.toString();
				off = le.end;
				dt.checkLimit(off);
				ret.end = off;
				ret.match = true;
				return ret;
			}
			if (le.letter == Ascii.Reverse_Solidus) {
				le = TletterLiteral.transportEscapeSequence(name, dt.text, off);
			}
			sb.appendCodePoint(le.letter);
			off = le.end;
		}
		var ms = "Defect pattern: " + name + ". No closing \" after begin offset: " + offset + ".";
		throw new DefectPattern(ms);
	}

//	public static final UnicodeText unicodeText = new UnicodeText();
//
//	public static final UnicodeText3 unicodeText3 = new UnicodeText3();

//	public static Odef transport(String name, Data dt, int offset) throws DefectLetter, DefectOffset, DefectPattern {
//		var ret = new Odef();
//		ret.keyword = dt.keywordDefText;
//		ret.begin = offset;
//		var off = offset;
//		var afterText = unicodeText.afterText(dt.text, off);
//		if (afterText > 0) {
//			ret.text = dt.text.getFromTo(off + unicodeText.limit.getLength(), afterText - unicodeText.limit.getLength());
//		} else {
//			afterText = unicodeText3.afterText(dt.text, off);
//			if (afterText > 0) {
//				ret.text = dt.text.getFromTo(off + unicodeText3.limit.getLength(), afterText - unicodeText3.limit.getLength());
//			}
//		}
//		if (ret.text == null) {
//			ret.match = false;
//			return ret;
//		}
//		ret.end = afterText;
//		ret.match = true;
//		return ret;
//	}

}