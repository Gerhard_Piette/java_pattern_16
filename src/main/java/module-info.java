module com.gitlab.gerhard_piette.pattern_16 {
	exports com.gitlab.gerhard_piette.pattern_16;
	requires transitive com.gitlab.gerhard_piette.text_6;
	requires transitive com.gitlab.gerhard_piette.ascii_1;
	requires transitive com.gitlab.gerhard_piette.integer_1;
	requires transitive com.gitlab.gerhard_piette.java_string_1;
}