package com.gitlab.gerhard_piette.pattern_16;


public class GenerateTests extends JavaPattern {

	@Override
	public String makeOclassPackage() {
		return "com.gitlab.gerhard_piette.pattern_16.test";
	}

	public static void main(String[] args) throws Exception {
		var gen = new GenerateTests();
		gen.makeFromFile("./test_pattern.u", "./src/test/java/com/gitlab/gerhard_piette/pattern_16/test");
	}
}
