package com.gitlab.gerhard_piette.pattern_16.test;

import com.gitlab.gerhard_piette.pattern_16.test.iclass.ItestCheck;
import org.junit.jupiter.api.Test;
import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.java_string_1.JavaString;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.text_6.StringText;

import static org.junit.jupiter.api.Assertions.assertTrue;


/**
 * testCheck (check a)
 */
public class OtestCheck {

	public static final Idea idea = Idea.testCheck;

	public int begin = -1;

	public int end = -1;

	public Oa oa = null;


	@Test
	public void test_1() throws DefectOffset, DefectLetter {
		var str = "a";
		var dt = new Data();
		dt.text = new StringText(str);
		var off = ItestCheck.read(dt, 0);
		assertTrue(off == 0);
		assertTrue(dt.otestCheck != null);
	}

	@Test
	public void test_2() throws DefectOffset, DefectLetter {
		var str = "b";
		var dt = new Data();
		dt.text = new StringText(str);
		var off = ItestCheck.read(dt, 0);
		assertTrue(off == -1);
		assertTrue(dt.otestCheck != null);
	}

	/**
	 * testCheck (check a)
	 */
	public String write(Data dt) throws DefectOffset {
		return "";
	}

	public String toString(int indentLevel, Data dt) throws DefectOffset {
		var ret = "";
		for (int i = 0; i < indentLevel; i++) {
			ret += i + "\t";
		}
		ret += this.idea.name();
		ret += " begin " + begin;
		ret += " end " + end;
		if (end >= begin) {
			ret += " text \"" + JavaString.addEscapeSequences(dt.text.getFromTo(begin, end).toString()) + "\"";
		}
		if (oa != null) {
			ret += "\n";
			ret += oa.toString(indentLevel + 1, dt);
		}
		return ret;
	}

}
