package com.gitlab.gerhard_piette.pattern_16.test.iclass;

import com.gitlab.gerhard_piette.pattern_16.test.OtestMaxTo;
import com.gitlab.gerhard_piette.pattern_16.test.Data;
import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.java_string_1.JavaString;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.text_6.Offset;
import com.gitlab.gerhard_piette.text_6.StringText;
import com.gitlab.gerhard_piette.text_6.Text;
import java.util.List;
import java.util.ArrayList;
import java.util.List;


/**
 * testMaxTo (maxto 2 4 a a)
 */
public class ItestMaxTo {

	/**
	 * testMaxTo (maxto 2 4 a a)
	 */
	public static int read(Data dt, int offset) throws DefectLetter, DefectOffset {
		var ret = new OtestMaxTo();
		ret.begin = offset;
		dt.otestMaxTo = ret;
		int off = offset;
		final int min = 2;
		final int max = 4;
		int count = 0;
		while (count < min) {
			off = Ia.read(dt, off);
			if (dt.oa != null) {
				ret.oaList.add(dt.oa);
			}
			if (off < 0) {
				dt.otestMaxTo = ret;
				ret.end = off;
				return off;
			}
			count++;
		}
		var listMark = ret.oaList.size();
		while (count < max) {
			var off2 = Ia.read(dt, off); 
			if (off2 >= 0) {
				listMark = ret.oaList.size();
				ret.end = off2;
				ret.oa = dt.oa;
			}
			var off1 = Ia.read(dt, off);
			if (off1 >= 0) {
				if (dt.oa != null) {
					ret.oaList.add(dt.oa);
				}
				off = off1;
				count++;
			} else {
				break;
			}
		}
		var off2 = Ia.read(dt, off); 
		if (off2 >= 0) {
			listMark = ret.oaList.size();
			ret.end = off2;
			ret.oa = dt.oa;
		}
		if (ret.end >= 0) {
			ret.oaList = new ArrayList<>(ret.oaList.subList(0, listMark));
		} else {
			ret.end = -off - 1;
		}
		dt.otestMaxTo = ret;
		return ret.end;
	}

	public String toString() {
		return "testMaxTo (maxto 2 4 a a)";
	}

}
