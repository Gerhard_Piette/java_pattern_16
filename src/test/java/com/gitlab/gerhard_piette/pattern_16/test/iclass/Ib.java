package com.gitlab.gerhard_piette.pattern_16.test.iclass;

import com.gitlab.gerhard_piette.pattern_16.test.Ob;
import com.gitlab.gerhard_piette.pattern_16.test.Data;
import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.java_string_1.JavaString;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.text_6.Offset;
import com.gitlab.gerhard_piette.text_6.StringText;
import com.gitlab.gerhard_piette.text_6.Text;
import java.util.List;
import java.util.ArrayList;
import java.util.List;


/**
 * b "b"
 */
public class Ib {

	public static Text text = new StringText("b");

	/**
	 * b "b"
	 */
	public static int read(Data dt, int offset) throws DefectLetter, DefectOffset {
		var ret = new Ob();
		ret.begin = offset;
		dt.ob = ret;
		var afterMatch = Offset.afterMatch(dt.text, offset, text, 0, text.getLength());
		ret.end = afterMatch;
		return afterMatch;
	}

	public String toString() {
		return "b \"b\"";
	}

}
