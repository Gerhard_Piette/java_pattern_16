package com.gitlab.gerhard_piette.pattern_16.test.iclass;

import com.gitlab.gerhard_piette.pattern_16.test.OtestMaybe;
import com.gitlab.gerhard_piette.pattern_16.test.Data;
import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.java_string_1.JavaString;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.text_6.Offset;
import com.gitlab.gerhard_piette.text_6.StringText;
import com.gitlab.gerhard_piette.text_6.Text;
import java.util.List;
import java.util.ArrayList;
import java.util.List;


/**
 * testMaybe (maybe a)
 */
public class ItestMaybe {

	/**
	 * testMaybe (maybe a)
	 */
	public static int read(Data dt, int offset) throws DefectLetter, DefectOffset {
		var ret = new OtestMaybe();
		ret.begin = offset;
		dt.otestMaybe = ret;
		var off = Ia.read(dt, offset);
		if (off > 0) {
			ret.end = off;
		} else {
			ret.end = offset;
		}
		dt.otestMaybe = ret;
		return ret.end;
	}

	public String toString() {
		return "testMaybe (maybe a)";
	}

}
