package com.gitlab.gerhard_piette.pattern_16.test;

import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.java_string_1.JavaString;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.text_6.Offset;
import com.gitlab.gerhard_piette.text_6.StringText;
import com.gitlab.gerhard_piette.text_6.Text;


/**
 * b "b"
 */
public class Ob {

	public static final Idea idea = Idea.b;

	public int begin = -1;

	public int end = -1;

	public static Text text = new StringText("b");


	/**
	 * b "b"
	 */
	public String write(Data dt) throws DefectOffset {
		return dt.text.getFromTo(begin, end).toString();
	}

	/**
	 * b "b"
	 */
	public static int read(Data dt, int offset) throws DefectLetter, DefectOffset {
		var ret = new Ob();
		ret.begin = offset;
		dt.ob = ret;
		var afterMatch = Offset.afterMatch(dt.text, offset, text, 0, text.getLength());
		ret.end = afterMatch;
		return afterMatch;
	}

	public String toString(int indentLevel, Data dt) throws DefectOffset {
		var ret = "";
		for (int i = 0; i < indentLevel; i++) {
			ret += i + "\t";
		}
		ret += this.idea.name();
		ret += " begin " + begin;
		ret += " end " + end;
		if (end >= begin) {
			ret += " text \"" + JavaString.addEscapeSequences(dt.text.getFromTo(begin, end).toString()) + "\"";
		}
		return ret;
	}

}
