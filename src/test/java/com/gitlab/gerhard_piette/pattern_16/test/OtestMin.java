package com.gitlab.gerhard_piette.pattern_16.test;

import org.junit.jupiter.api.Test;
import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.java_string_1.JavaString;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.pattern_16.test.iclass.ItestMin;
import com.gitlab.gerhard_piette.text_6.StringText;


import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;


/**
 * testMin (min 2 a)
 */
public class OtestMin {

	public static final Idea idea = Idea.testMin;

	public int begin = -1;

	public int end = -1;

	public List<Oa> oaList = new ArrayList<>();


	@Test
	public void test_1() throws DefectOffset, DefectLetter {
		var str = "ab";
		var dt = new Data();
		dt.text = new StringText(str);
		var off = ItestMin.read(dt, 0);
		assertTrue(off == -2);
		assertTrue(dt.otestMin != null);
	}

	@Test
	public void test_2() throws DefectOffset, DefectLetter {
		var str = "aab";
		var dt = new Data();
		dt.text = new StringText(str);
		var off = ItestMin.read(dt, 0);
		assertTrue(off == 2);
		assertTrue(dt.otestMin != null);
	}

	@Test
	public void test_3() throws DefectOffset, DefectLetter {
		var str = "aaaaaaaaa";
		var dt = new Data();
		dt.text = new StringText(str);
		var off = ItestMin.read(dt, 0);
		assertTrue(off == 2);
		assertTrue(dt.otestMin != null);
	}

	/**
	 * testMin (min 2 a)
	 */
	public String write(Data dt) throws DefectOffset {
		var ret = "";
		for (var ta : oaList) {
			ret += ta.write(dt);
		}
		return ret;
	}


	public String toString(int indentLevel, Data dt) throws DefectOffset {
		var ret = "";
		for (int i = 0; i < indentLevel; i++) {
			ret += i + "\t";
		}
		ret += this.idea.name();
		ret += " begin " + begin;
		ret += " end " + end;
		if (end >= begin) {
			ret += " text \"" + JavaString.addEscapeSequences(dt.text.getFromTo(begin, end).toString()) + "\"";
		}
		for (var x : oaList) {
			ret += "\n";
			ret += x.toString(indentLevel + 1, dt);
		}
		return ret;
	}

}
