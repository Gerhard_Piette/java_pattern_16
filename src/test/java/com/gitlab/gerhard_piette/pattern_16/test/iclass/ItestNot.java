package com.gitlab.gerhard_piette.pattern_16.test.iclass;

import com.gitlab.gerhard_piette.pattern_16.test.OtestNot;
import com.gitlab.gerhard_piette.pattern_16.test.Data;
import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.java_string_1.JavaString;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.text_6.Offset;
import com.gitlab.gerhard_piette.text_6.StringText;
import com.gitlab.gerhard_piette.text_6.Text;
import java.util.List;
import java.util.ArrayList;
import java.util.List;


/**
 * testNot (not a)
 */
public class ItestNot {

	/**
	 * testNot (not a)
	 */
	public static int read(Data dt, int offset) throws DefectLetter, DefectOffset {
		var ret = new OtestNot();
		ret.begin = offset;
		var off = Ia.read(dt, offset);
		if (off < 0) {
			ret.end = offset;
		}
		dt.otestNot = ret;
		return ret.end;
	}

	public String toString() {
		return "testNot (not a)";
	}

}
