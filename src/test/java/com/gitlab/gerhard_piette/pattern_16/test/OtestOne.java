package com.gitlab.gerhard_piette.pattern_16.test;

import org.junit.jupiter.api.Test;
import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.java_string_1.JavaString;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.pattern_16.test.iclass.ItestOne;
import com.gitlab.gerhard_piette.text_6.StringText;

import static org.junit.jupiter.api.Assertions.assertTrue;


/**
 * testOne (one a b)
 */
public class OtestOne {

	public static final Idea idea = Idea.testOne;

	public int begin = -1;

	public int end = -1;


	public Oa oa = null;

	public Ob ob = null;

	@Test
	public void test_1() throws DefectOffset, DefectLetter {
		var str = "a";
		var dt = new Data();
		dt.text = new StringText(str);
		var off = ItestOne.read(dt, 0);
		assertTrue(off == 1);
		assertTrue(dt.otestOne != null);
	}

	@Test
	public void test_2() throws DefectOffset, DefectLetter {
		var str = "b";
		var dt = new Data();
		dt.text = new StringText(str);
		var off = ItestOne.read(dt, 0);
		assertTrue(off == 1);
		assertTrue(dt.otestOne != null);
	}

	@Test
	public void test_3() throws DefectOffset, DefectLetter {
		var str = "c";
		var dt = new Data();
		dt.text = new StringText(str);
		var off = ItestOne.read(dt, 0);
		assertTrue(off == -1);
		assertTrue(dt.otestOne != null);
	}

	/**
	 * testOne (one a b)
	 */
	public String write(Data dt) throws DefectOffset {
		if (oa.end >= 0) {
			var ret = oa.write(dt);
			return ret;
		}
		if (ob.end >= 0) {
			var ret = ob.write(dt);
			return ret;
		}
		throw new UnsupportedOperationException();
	}


	public String toString(int indentLevel, Data dt) throws DefectOffset {
		var ret = "";
		for (int i = 0; i < indentLevel; i++) {
			ret += i + "\t";
		}
		ret += this.idea.name();
		ret += " begin " + begin;
		ret += " end " + end;
		if (end >= begin) {
			ret += " text \"" + JavaString.addEscapeSequences(dt.text.getFromTo(begin, end).toString()) + "\"";
		}
		if (oa != null) {
			ret += "\n";
			ret += oa.toString(indentLevel + 1, dt);
		}
		if (ob != null) {
			ret += "\n";
			ret += ob.toString(indentLevel + 1, dt);
		}
		return ret;
	}

}
