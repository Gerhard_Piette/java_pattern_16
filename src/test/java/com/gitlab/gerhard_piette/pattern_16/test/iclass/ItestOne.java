package com.gitlab.gerhard_piette.pattern_16.test.iclass;

import com.gitlab.gerhard_piette.pattern_16.test.OtestOne;
import com.gitlab.gerhard_piette.pattern_16.test.Data;
import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.java_string_1.JavaString;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.text_6.Offset;
import com.gitlab.gerhard_piette.text_6.StringText;
import com.gitlab.gerhard_piette.text_6.Text;
import java.util.List;
import java.util.ArrayList;
import java.util.List;


/**
 * testOne (one a b)
 */
public class ItestOne {

	/**
	 * testOne (one a b)
	 */
	public static int read(Data dt, int offset) throws DefectLetter, DefectOffset {
		var ret = new OtestOne();
		ret.begin = offset;
		var off = offset;
		off = Ia.read(dt, offset);
		ret.oa = dt.oa;
		if (off >= 0) {
			dt.otestOne = ret;
			ret.end = off;
			return off;
		}
		off = Ib.read(dt, offset);
		ret.ob = dt.ob;
		if (off >= 0) {
			dt.otestOne = ret;
			ret.end = off;
			return off;
		}
		dt.otestOne = ret;
		ret.end = off;
		return off;
	}

	public String toString() {
		return "testOne (one a b)";
	}

}
