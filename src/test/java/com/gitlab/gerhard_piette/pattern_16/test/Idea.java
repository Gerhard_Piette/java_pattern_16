package com.gitlab.gerhard_piette.pattern_16.test;

public enum Idea {
	a,
	b,
	testAll,
	testCheck,
	testCopy,
	testEnd,
	testMax,
	testMaxTo,
	testMaybe,
	testMin,
	testMinTo,
	testNot,
	testOne,
}
