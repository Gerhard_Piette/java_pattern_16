package com.gitlab.gerhard_piette.pattern_16.test.iclass;

import com.gitlab.gerhard_piette.pattern_16.test.OtestMin;
import com.gitlab.gerhard_piette.pattern_16.test.Data;
import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.java_string_1.JavaString;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.text_6.Offset;
import com.gitlab.gerhard_piette.text_6.StringText;
import com.gitlab.gerhard_piette.text_6.Text;
import java.util.List;
import java.util.ArrayList;
import java.util.List;


/**
 * testMin (min 2 a)
 */
public class ItestMin {

	/**
	 * testMin (min 2 a)
	 */
	public static int read(Data dt, int offset) throws DefectLetter, DefectOffset {
		var ret = new OtestMin();
		ret.begin = offset;
		dt.otestMin = ret;
		int off = offset;
		final int min = 2;
		int count = 0;
		while (count < min) {
			off = Ia.read(dt, off);
			if (dt.oa != null) {
				ret.oaList.add(dt.oa);
			}
			if (off < 0) {
				dt.otestMin = ret;
				ret.end = off;
				return off;
			}
			count++;
		}
		dt.otestMin = ret;
		ret.end = off;
		return off;
	}

	public String toString() {
		return "testMin (min 2 a)";
	}

}
