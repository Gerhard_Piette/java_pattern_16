package com.gitlab.gerhard_piette.pattern_16.test.iclass;

import com.gitlab.gerhard_piette.pattern_16.test.OtestMax;
import com.gitlab.gerhard_piette.pattern_16.test.Data;
import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.java_string_1.JavaString;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.text_6.Offset;
import com.gitlab.gerhard_piette.text_6.StringText;
import com.gitlab.gerhard_piette.text_6.Text;
import java.util.List;
import java.util.ArrayList;
import java.util.List;


/**
 * testMax (max 2 4 a)
 */
public class ItestMax {

	/**
	 * testMax (max 2 4 a)
	 */
	public static int read(Data dt, int offset) throws DefectLetter, DefectOffset {
		var ret = new OtestMax();
		ret.begin = offset;
		int off = offset;
		final int min = 2;
		final int max = 4;
		int count = 0;
		while (count < min) {
			off = Ia.read(dt, off);
			if (dt.oa != null) {
				ret.oaList.add(dt.oa);
			}
			if (off < 0) {
				dt.otestMax = ret;
				ret.end = off;
				return off;
			}
			count++;
		}
		while (count < max) {
			var off2 = Ia.read(dt, off);
			if (off2 < 0) {
				break;
			}
			if (dt.oa != null) {
				ret.oaList.add(dt.oa);
			}
			off = off2;
			count++;
		}
		dt.otestMax = ret;
		ret.end = off;
		return off;
	}

	public String toString() {
		return "testMax (max 2 4 a)";
	}

}
