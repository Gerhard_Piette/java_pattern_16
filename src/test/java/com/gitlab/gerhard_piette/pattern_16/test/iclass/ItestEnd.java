package com.gitlab.gerhard_piette.pattern_16.test.iclass;

import com.gitlab.gerhard_piette.pattern_16.test.OtestEnd;
import com.gitlab.gerhard_piette.pattern_16.test.Data;
import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.java_string_1.JavaString;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.text_6.Offset;
import com.gitlab.gerhard_piette.text_6.StringText;
import com.gitlab.gerhard_piette.text_6.Text;
import java.util.List;
import java.util.ArrayList;
import java.util.List;


/**
 * testEnd (end)
 */
public class ItestEnd {

	/**
	 * testEnd (end)
	 */
	public static int read(Data dt, int offset) throws DefectLetter, DefectOffset {
		if (offset >= dt.text.getLength()) {
			return offset;
		}
		return -offset - 1;
	}

	public String toString() {
		return "testEnd (end)";
	}

}
