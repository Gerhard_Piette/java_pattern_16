package com.gitlab.gerhard_piette.pattern_16.test;

import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.letter_1.LetterEnd;
import com.gitlab.gerhard_piette.text_6.Text;

public class Data {

	public Text text;

	public LetterEnd read(int offset) throws DefectLetter, DefectOffset {
		var ret = text.read(offset);
		return ret;
	}

	public Oa oa = null;

	public Ob ob = null;

	public OtestAll otestAll = null;

	public OtestCheck otestCheck = null;

	public OtestCopy otestCopy = null;

	public OtestEnd otestEnd = null;

	public OtestMax otestMax = null;

	public OtestMaxTo otestMaxTo = null;

	public OtestMaybe otestMaybe = null;

	public OtestMin otestMin = null;

	public OtestMinTo otestMinTo = null;

	public OtestNot otestNot = null;

	public OtestOne otestOne = null;

}
