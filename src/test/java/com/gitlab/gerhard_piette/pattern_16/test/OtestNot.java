package com.gitlab.gerhard_piette.pattern_16.test;

import com.gitlab.gerhard_piette.pattern_16.test.iclass.ItestNot;
import org.junit.jupiter.api.Test;
import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.java_string_1.JavaString;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.text_6.StringText;

import static org.junit.jupiter.api.Assertions.assertTrue;


/**
 * testNot (not a)
 */
public class OtestNot {

	public static final Idea idea = Idea.testNot;

	public int begin = -1;

	public int end = -1;

	public Oa ta = null;

	@Test
	public void test_1() throws DefectOffset, DefectLetter {
		var str = "b";
		var dt = new Data();
		dt.text = new StringText(str);
		var off = ItestNot.read(dt, 0);
		assertTrue(off == 0);
		assertTrue(dt.otestNot != null);
	}

	@Test
	public void test_2() throws DefectOffset, DefectLetter {
		var str = "a";
		var dt = new Data();
		dt.text = new StringText(str);
		var off = ItestNot.read(dt, 0);
		assertTrue(off == -1);
		assertTrue(dt.otestNot != null);
	}

	/**
	 * testNot (not a)
	 */
	public String write(Data dt) throws DefectOffset {
		return "";
	}

	public String toString(int indentLevel, Data dt) throws DefectOffset {
		var ret = "";
		for (int i = 0; i < indentLevel; i++) {
			ret += i + "\t";
		}
		ret += this.idea.name();
		ret += " begin " + begin;
		ret += " end " + end;
		if (end >= begin) {
			ret += " text \"" + JavaString.addEscapeSequences(dt.text.getFromTo(begin, end).toString()) + "\"";
		}
		if (ta != null) {
			ret += "\n";
			ret += ta.toString(indentLevel + 1, dt);
		}
		return ret;
	}

}
