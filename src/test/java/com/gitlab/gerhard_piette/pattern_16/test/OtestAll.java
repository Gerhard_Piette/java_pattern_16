package com.gitlab.gerhard_piette.pattern_16.test;

import com.gitlab.gerhard_piette.pattern_16.test.iclass.ItestAll;
import org.junit.jupiter.api.Test;
import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.java_string_1.JavaString;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.text_6.StringText;

import static org.junit.jupiter.api.Assertions.assertTrue;


/**
 * testAll (all a b a)
 */
public class OtestAll {

	public static final Idea idea = Idea.testAll;

	public int begin = -1;

	public int end = -1;

	public Oa oa = null;

	public Ob ob = null;

	public Oa oa_2 = null;


	@Test
	public void test_1() throws DefectOffset, DefectLetter {
		var str = "aba";
		var dt = new Data();
		dt.text = new StringText(str);
		var off = ItestAll.read(dt, 0);
		assertTrue(off == 3);
		assertTrue(dt.otestAll != null);
//		System.out.println(dt.otestAll.toString(0, dt));
	}

	@Test
	public void test_2() throws DefectOffset, DefectLetter {
		var str = "aa";
		var dt = new Data();
		dt.text = new StringText(str);
		var off = ItestAll.read(dt, 0);
		assertTrue(off == -2);
		assertTrue(dt.otestAll != null);
//		System.out.println(dt.otestAll.toString(0, dt));
	}

	/**
	 * testAll (all a b a)
	 */
	public String write(Data dt) throws DefectOffset {
		var ret = "";
		if (oa.end >= 0) {
			ret += oa.write(dt);
		} else {
			throw new UnsupportedOperationException();
		}
		if (ob.end >= 0) {
			ret += ob.write(dt);
		} else {
			throw new UnsupportedOperationException();
		}
		if (oa_2.end >= 0) {
			ret += oa_2.write(dt);
		} else {
			throw new UnsupportedOperationException();
		}
		return ret;
	}

	public String toString(int indentLevel, Data dt) throws DefectOffset {
		var ret = "";
		for (int i = 0; i < indentLevel; i++) {
			ret += i + "\t";
		}
		ret += this.idea.name();
		ret += " begin " + begin;
		ret += " end " + end;
		if (end >= begin) {
			ret += " text \"" + JavaString.addEscapeSequences(dt.text.getFromTo(begin, end).toString()) + "\"";
		}
		if (oa != null) {
			ret += "\n";
			ret += oa.toString(indentLevel + 1, dt);
		}
		if (ob != null) {
			ret += "\n";
			ret += ob.toString(indentLevel + 1, dt);
		}
		if (oa_2 != null) {
			ret += "\n";
			ret += oa_2.toString(indentLevel + 1, dt);
		}
		return ret;
	}

}
