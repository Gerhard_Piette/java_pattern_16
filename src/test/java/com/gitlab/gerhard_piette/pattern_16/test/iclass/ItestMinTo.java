package com.gitlab.gerhard_piette.pattern_16.test.iclass;

import com.gitlab.gerhard_piette.pattern_16.test.OtestMinTo;
import com.gitlab.gerhard_piette.pattern_16.test.Data;
import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.java_string_1.JavaString;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.text_6.Offset;
import com.gitlab.gerhard_piette.text_6.StringText;
import com.gitlab.gerhard_piette.text_6.Text;
import java.util.List;
import java.util.ArrayList;
import java.util.List;


/**
 * testMinTo (minto 2 4 a b)
 */
public class ItestMinTo {

	/**
	 * testMinTo (minto 2 4 a b)
	 */
	public static int read(Data dt, int offset) throws DefectLetter, DefectOffset {
		var ret = new OtestMinTo();
		ret.begin = offset;
		dt.otestMinTo = ret;
		int off = offset;
		final int min = 2;
		final int max = 4;
		int count = 0;
		while (count < min) {
			off = Ia.read(dt, off);
			if (dt.oa != null) {
				ret.oaList.add(dt.oa);
			}
			if (off < 0) {
				dt.otestMinTo = ret;
				ret.end = off;
				return off;
			}
			count++;
		}
		while (count < max) {
			var off2 = Ib.read(dt, off); 
			if (off2 >= 0) {
				ret.ob = dt.ob;
				dt.otestMinTo = ret;
				ret.end = off2;
				return off2;
			}
			var off1 = Ia.read(dt, off);
			if (off1 >= 0) {
				if (dt.oa != null) {
					ret.oaList.add(dt.oa);
				}
				off = off1;
				count++;
			} else {
				break;
			}
		}
		var off2 = Ib.read(dt, off); 
		if (off2 >= 0) {
			ret.ob = dt.ob;
			dt.otestMinTo = ret;
			ret.end = off2;
			return off2;
		}
		dt.otestMinTo = ret;
		ret.end = -off - 1;
		return ret.end;
	}

	public String toString() {
		return "testMinTo (minto 2 4 a b)";
	}

}
