package com.gitlab.gerhard_piette.pattern_16.test;

import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.java_string_1.JavaString;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;


/**
 * a = 'a'
 */
public class Oa {

	public static final Idea idea = Idea.a;

	public int begin = -1;

	public int end = -1;

	public int letter = -1;


	/**
	 * a = 'a'
	 */
	public String write(Data dt) throws DefectOffset {
		return dt.text.getFromTo(begin, end).toString();
	}

	/**
	 * a = 'a'
	 */
	public static int read(Data dt, int offset) throws DefectLetter, DefectOffset {
		var le = dt.read(offset);
		// 'a' = 97
		if (le.letter == 97) {
			return le.end;
		}
		return -offset - 1;
	}

	public String toString(int indentLevel, Data dt) throws DefectOffset {
		var ret = "";
		for (int i = 0; i < indentLevel; i++) {
			ret += i + "\t";
		}
		ret += this.idea.name();
		ret += " begin " + begin;
		ret += " end " + end;
		if (end >= begin) {
			ret += " text \"" + JavaString.addEscapeSequences(dt.text.getFromTo(begin, end).toString()) + "\"";
		}
		return ret;
	}

}
