package com.gitlab.gerhard_piette.pattern_16.test.iclass;

import com.gitlab.gerhard_piette.pattern_16.test.OtestCheck;
import com.gitlab.gerhard_piette.pattern_16.test.Data;
import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.java_string_1.JavaString;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.text_6.Offset;
import com.gitlab.gerhard_piette.text_6.StringText;
import com.gitlab.gerhard_piette.text_6.Text;
import java.util.List;
import java.util.ArrayList;
import java.util.List;


/**
 * testCheck (check a)
 */
public class ItestCheck {

	/**
	 * testCheck (check a)
	 */
	public static int read(Data dt, int offset) throws DefectLetter, DefectOffset {
		var ret = new OtestCheck();
		ret.begin = offset;
		ret.end = Ia.read(dt, offset);
		if (ret.end >= 0) {
			ret.end = offset;
		}
		dt.otestCheck = ret;
		return ret.end;
	}

	public String toString() {
		return "testCheck (check a)";
	}

}
