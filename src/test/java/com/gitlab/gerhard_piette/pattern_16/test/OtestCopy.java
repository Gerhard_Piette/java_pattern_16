package com.gitlab.gerhard_piette.pattern_16.test;

import org.junit.jupiter.api.Test;
import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.java_string_1.JavaString;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.pattern_16.test.iclass.ItestCopy;
import com.gitlab.gerhard_piette.text_6.StringText;

import static org.junit.jupiter.api.Assertions.assertTrue;


/**
 * testCopy testAll
 */
public class OtestCopy {

	public static final Idea idea = Idea.testCopy;

	public int begin = -1;

	public int end = -1;

	public OtestAll otestAll = null;


	@Test
	public void test_1() throws DefectOffset, DefectLetter {
		var str = "aba";
		var dt = new Data();
		dt.text = new StringText(str);
		var off = ItestCopy.read(dt, 0);
		assertTrue(off == 3);
		assertTrue(dt.otestCopy != null);
	}

	@Test
	public void test_2() throws DefectOffset, DefectLetter {
		var str = "aa";
		var dt = new Data();
		dt.text = new StringText(str);
		var off = ItestCopy.read(dt, 0);
		assertTrue(off == -2);
		assertTrue(dt.otestCopy != null);
	}

	/**
	 * testCopy testAll
	 */
	public String write(Data dt) throws DefectOffset {
		var ret = "";
		if (otestAll.end >= 0) {
			ret += otestAll.write(dt);
		} else {
			throw new UnsupportedOperationException();
		}
		return ret;
	}


	public String toString(int indentLevel, Data dt) throws DefectOffset {
		var ret = "";
		for (int i = 0; i < indentLevel; i++) {
			ret += i + "\t";
		}
		ret += this.idea.name();
		ret += " begin " + begin;
		ret += " end " + end;
		if (end >= begin) {
			ret += " text \"" + JavaString.addEscapeSequences(dt.text.getFromTo(begin, end).toString()) + "\"";
		}
		if (otestAll != null) {
			ret += "\n";
			ret += otestAll.toString(indentLevel + 1, dt);
		}
		return ret;
	}

}
