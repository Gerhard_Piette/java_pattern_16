package com.gitlab.gerhard_piette.pattern_16.test.iclass;

import com.gitlab.gerhard_piette.pattern_16.test.OtestAll;
import com.gitlab.gerhard_piette.pattern_16.test.Data;
import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.java_string_1.JavaString;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.text_6.Offset;
import com.gitlab.gerhard_piette.text_6.StringText;
import com.gitlab.gerhard_piette.text_6.Text;
import java.util.List;
import java.util.ArrayList;
import java.util.List;


/**
 * testAll (all a b a)
 */
public class ItestAll {

	/**
	 * testAll (all a b a)
	 */
	public static int read(Data dt, int offset) throws DefectLetter, DefectOffset {
		var ret = new OtestAll();
		ret.begin = offset;
		int off = offset;
		off = Ia.read(dt, off);
		ret.oa = dt.oa;
		if (off < 0) {
			dt.otestAll = ret;
			ret.end = off;
			return off;
		}
		off = Ib.read(dt, off);
		ret.ob = dt.ob;
		if (off < 0) {
			dt.otestAll = ret;
			ret.end = off;
			return off;
		}
		off = Ia.read(dt, off);
		ret.oa_2 = dt.oa;
		if (off < 0) {
			dt.otestAll = ret;
			ret.end = off;
			return off;
		}
		dt.otestAll = ret;
		ret.end = off;
		return off;
	}

	public String toString() {
		return "testAll (all a b a)";
	}

}
