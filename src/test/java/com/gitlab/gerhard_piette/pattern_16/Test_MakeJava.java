package com.gitlab.gerhard_piette.pattern_16;

import org.junit.jupiter.api.Test;
import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.text_6.StringText;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class Test_MakeJava {

	public JavaPattern makeJava = new JavaPattern();

	public String folderPath = "./generated";

	public Data makeData(String source) {
		var text = new StringText(source);
		var data = new Data();
		data.text = text;
		return data;
	}

	@Test
	public void test_1() throws DefectPattern, DefectOffset, DefectLetter, DefectFile, DefectGenerator, IOException, DefectLimit {
		var str = "";
		str += "textBeginLetter = '{'";
		str += "\n";
		str += "textEndLetter = '}'";
		str += "\n";
		str += "textEscape1 \"'{\"";
		str += "\n";
		str += "textEscape2 \"'}\"";
		str += "\n";
		str += "textLetter >= 0";
		str += "\n";
		str += "textPart (one textEscape1 textEscape2 textLetter)";
		str += "\n";
		str += "textEnd (maxto textPart textEndLetter)";
		str += "\n";
		str += "textLiteral (all textBeginLetter textEnd)";
		makeJava.makeFromString(str, null);
//		System.out.println(data);
//		System.out.println(oMaxStatement);
	}

	@Test
	public void test_copy_1() throws DefectPattern, DefectOffset, DefectLetter, DefectFile, DefectGenerator, IOException, DefectLimit {
		var str = "";
		str += "A = '{'";
		str += "\n";
		str += "B A";
		makeJava.makeFromString(str, null);
//		System.out.println(data);
//		System.out.println(oMaxStatement);
	}

	@Test
	public void test_text_1() throws DefectPattern, DefectOffset, DefectLetter, DefectFile, DefectGenerator, IOException, DefectLimit {
		var str = "letnumS \"''s\"";
		makeJava.makeFromString(str, null);
//		System.out.println(data);
//		System.out.println(oMaxStatement);
	}
}