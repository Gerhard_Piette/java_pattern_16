package com.gitlab.gerhard_piette.pattern_16;

import org.junit.jupiter.api.Test;
import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.text_6.StringText;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class Test_TdefText {

	public TmaxStatement tMaxStatement = new TmaxStatement();

	public String folderPath = "./generated";

	public Data makeData(String source) {
		var text = new StringText(source);
		var data = new Data();
		data.text = text;
		return data;
	}

	@Test
	public void test_1() throws DefectLetter, DefectOffset, DefectPattern, DefectLimit {
		var str = "letnumS \"''s\"";
		var data = makeData(str);
		var oMaxStatement = tMaxStatement.transport(data, 0);
		assertTrue(oMaxStatement.statementList.get(0).oDef.keyword == data.keywordText);
//		System.out.println(data);
//		System.out.println(oMaxStatement);
	}

	@Test
	public void test_2() throws DefectLetter, DefectOffset, DefectPattern, DefectLimit {
		var str = "n8 (all \"8n\" limGap)";
		var data = makeData(str);
		var oMaxStatement = tMaxStatement.transport(data, 0);
//		System.out.println(oMaxStatement.statementList.size() == 2);
//		System.out.println(data);
//		System.out.println(oMaxStatement);
	}

}