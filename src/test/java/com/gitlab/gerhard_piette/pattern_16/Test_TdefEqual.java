package com.gitlab.gerhard_piette.pattern_16;

import org.junit.jupiter.api.Test;
import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.text_6.StringText;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class Test_TdefEqual {

	public TmaxStatement tMaxStatement = new TmaxStatement();

	public String folderPath = "./generated";

	public Data makeData(String source) {
		var text = new StringText(source);
		var data = new Data();
		data.text = text;
		return data;
	}

	@Test
	public void test_1() throws DefectLetter, DefectOffset, DefectPattern, DefectLimit {
		var str = "A = 1";
		var data = makeData(str);
		var oMaxStatement = tMaxStatement.transport(data, 0);
//		System.out.println(data);
//		System.out.println(oMaxStatement);
	}

	@Test
	public void test_2() throws DefectLetter, DefectOffset, DefectPattern, DefectLimit {
		var str = "A = 'a'";
		var data = makeData(str);
//		data.printOffsetAndLetter();
		var oMaxStatement = tMaxStatement.transport(data, 0);
//		System.out.println(data);
//		System.out.println(oMaxStatement);
//		System.out.println(data);
	}

	@Test
	public void test_3() throws DefectLetter, DefectOffset, DefectLimit {
		try {
			var str = "A = B";
			var data = makeData(str);
			var oMaxStatement = tMaxStatement.transport(data, 0);
			assertTrue(false);
		} catch (DefectPattern dp) {
			assertTrue(true);
		}
	}

	@Test
	public void test_4() throws DefectLetter, DefectOffset, DefectPattern, DefectLimit {
		var str = "A = 1 B = 'b' C = B";
		var data = makeData(str);
		var oMaxStatement = tMaxStatement.transport(data, 0);
//		System.out.println(data);
//		System.out.println(oMaxStatement);
	}

	@Test
	public void test_5() throws DefectLetter, DefectOffset, DefectPattern, DefectLimit {
		var str = "A = 1 B = 'b' C = B";
		var data = makeData(str);
		var oMaxStatement = tMaxStatement.transport(data, 0);
		assertTrue(oMaxStatement.statementMap.get("C").oDef.letter.number == 'b');
//		System.out.println(data);
//		System.out.println(oMaxStatement);
	}

}