package com.gitlab.gerhard_piette.pattern_16;

import org.junit.jupiter.api.Test;
import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.text_6.StringText;

public class Test_TdefCopy {

	public TmaxStatement tMaxStatement = new TmaxStatement();

	public String folderPath = "./generated";

	public Data makeData(String source) {
		var text = new StringText(source);
		var data = new Data();
		data.text = text;
		return data;
	}

	@Test
	public void test_1() throws DefectLetter, DefectOffset, DefectPattern, DefectLimit {
		var str = "A B";
		var data = makeData(str);
		var oMaxStatement = tMaxStatement.transport(data, 0);
//		var jp = new JavaPattern();
//		jp.checkUsedPatterns(oMaxStatement, data);
//		System.out.println(data);
//		System.out.println(oMaxStatement);
	}

}